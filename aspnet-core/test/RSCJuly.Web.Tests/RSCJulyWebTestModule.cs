﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using RSCJuly.EntityFrameworkCore;
using RSCJuly.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace RSCJuly.Web.Tests
{
    [DependsOn(
        typeof(RSCJulyWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class RSCJulyWebTestModule : AbpModule
    {
        public RSCJulyWebTestModule(RSCJulyEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(RSCJulyWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(RSCJulyWebMvcModule).Assembly);
        }
    }
}