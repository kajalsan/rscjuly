﻿using System.Threading.Tasks;
using RSCJuly.Models.TokenAuth;
using RSCJuly.Web.Controllers;
using Shouldly;
using Xunit;

namespace RSCJuly.Web.Tests.Controllers
{
    public class HomeController_Tests: RSCJulyWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}