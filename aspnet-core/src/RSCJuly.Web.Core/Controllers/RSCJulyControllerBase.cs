using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace RSCJuly.Controllers
{
    public abstract class RSCJulyControllerBase: AbpController
    {
        protected RSCJulyControllerBase()
        {
            LocalizationSourceName = RSCJulyConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
