﻿using Abp.AutoMapper;
using RSCJuly.Authentication.External;

namespace RSCJuly.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
