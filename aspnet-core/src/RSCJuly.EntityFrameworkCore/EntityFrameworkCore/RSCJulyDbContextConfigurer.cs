using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace RSCJuly.EntityFrameworkCore
{
    public static class RSCJulyDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<RSCJulyDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<RSCJulyDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
