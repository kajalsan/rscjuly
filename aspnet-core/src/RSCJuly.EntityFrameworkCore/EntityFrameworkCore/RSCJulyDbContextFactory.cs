﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using RSCJuly.Configuration;
using RSCJuly.Web;

namespace RSCJuly.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class RSCJulyDbContextFactory : IDesignTimeDbContextFactory<RSCJulyDbContext>
    {
        public RSCJulyDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<RSCJulyDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            RSCJulyDbContextConfigurer.Configure(builder, configuration.GetConnectionString(RSCJulyConsts.ConnectionStringName));

            return new RSCJulyDbContext(builder.Options);
        }
    }
}
