﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using RSCJuly.Authorization.Roles;
using RSCJuly.Authorization.Users;
using RSCJuly.MultiTenancy;
using RSCJuly.Parameters;
using System.Configuration;

namespace RSCJuly.EntityFrameworkCore
{
    public class RSCJulyDbContext : AbpZeroDbContext<Tenant, Role, User, RSCJulyDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<ProtectiveMeasure> ProtectiveMeasures { get; set; }
        public virtual DbSet<Hazard> Hazards { get; set; }
        public virtual DbSet<Subtask> Subtasks { get; set; }
        public virtual DbSet<Safeguard> Safeguards { get; set; }
        public virtual DbSet<Signup.Signup> Signups { get; set; }
        public virtual DbSet<Company.Company> Companys { get; set; }
        public virtual DbSet<RobotSystem.RobotSystem> RobotSystems { get; set; }
        public virtual DbSet<RobotTask.RobotTask> RobotTasks { get; set; }
        public virtual DbSet<Locations.Locations> Locations { get; set; }
        public virtual DbSet<RobotTask.UploadDocument> UploadDocuments { get; set; }

        public virtual DbSet<UserPortal.UserPortal> UserPortals { get; set; }
        
        public RSCJulyDbContext(DbContextOptions<RSCJulyDbContext> options)
            : base(options)
        {
        }
    }
}
