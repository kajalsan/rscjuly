﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using RSCJuly.Configuration;

namespace RSCJuly.Web.Host.Startup
{
    [DependsOn(
       typeof(RSCJulyWebCoreModule))]
    public class RSCJulyWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public RSCJulyWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(RSCJulyWebHostModule).GetAssembly());
        }
    }
}
