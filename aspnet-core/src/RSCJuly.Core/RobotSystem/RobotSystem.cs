﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RSCJuly.RobotSystem
{
    [Table("RobotSystems")]
  public  class RobotSystem : FullAuditedEntity
    {
        public int CompanyId { get; set; }
        public string RobotName { get; set; }
        public string BuildingLocation { get; set; }
        public string Asset { get; set; }
        public string IntegratorManufacturer { get; set; }
        public string DateOrigin { get; set; }

        public string LastRevision { get; set; }
        public string ReasonRevision { get; set; }

        public string ProjectManager { get; set; }
        public string RiskAssessmentValidator { get; set; }
    }
}
