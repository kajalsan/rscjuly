﻿using Abp.Domain.Entities.Auditing;
using RSCJuly.Authorization.Users;
using RSCJuly.Parameters;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RSCJuly.Company
{
    [Table("Company")]
    public class Company : FullAuditedEntity
    {
        public const int MaxNameLength = 100;

        public const int MaxCSCZLength = 50;

        public const int MaxLocationLength = 150;

        [ForeignKey(nameof(User))]
        public int UserId { get; set; }

        [StringLength(MaxNameLength)]
        public string CompanyName { get; set; }

        [StringLength(MaxCSCZLength)]
        public string Phoneno { get; set; }

        [StringLength(MaxCSCZLength)]
        public string subscriptiontype { get; set; }

        [StringLength(MaxCSCZLength)]
        public string country { get; set; }

        [StringLength(MaxCSCZLength)]
        public string state { get; set; }

        [StringLength(MaxCSCZLength)]
        public string city { get; set; }

        [StringLength(MaxLocationLength)]
        public string Address { get; set; }

        [StringLength(MaxCSCZLength)]
        public string zip { get; set; }


        public virtual ICollection<Locations.Locations> Locations { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<Subtask> Subtasks { get; set; }
        public virtual ICollection<Hazard> Hazards { get; set; }
        public virtual ICollection<ProtectiveMeasure> ProtectiveMeasures { get; set; }
        public virtual ICollection<UserPortal.UserPortal> Userportals { get; set; }


    }
}