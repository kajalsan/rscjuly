﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RSCJuly.Locations
{
    [Table("Locations")]
    public class Locations: FullAuditedEntity
    {

        public const int MaxCSCZLength = 50;

        public const int MaxLocationLength = 150;

        [ForeignKey(nameof(Company))]
        public int CompanyId { get; set; }

     //   public int UserportalId { get; set; }

        [StringLength(MaxCSCZLength)]
        public string LocationName { get; set; }
        public string country { get; set; }

        [StringLength(MaxCSCZLength)]
        public string state { get; set; }

        [StringLength(MaxCSCZLength)]
        public string city { get; set; }

        [StringLength(MaxLocationLength)]
        public string Address { get; set; }

        [StringLength(MaxCSCZLength)]
        public string zip { get; set; }

       
    }
}
