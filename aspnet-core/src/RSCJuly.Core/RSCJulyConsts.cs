﻿namespace RSCJuly
{
    public class RSCJulyConsts
    {
        public const string LocalizationSourceName = "RSCJuly";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
