﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RSCJuly.RobotTask
{

    [Table("RobotTasks")]
    public class RobotTask : AuditedEntity
    {
        public int CompanyId { get; set; }
        public string TaskId { get; set; }
        public string SubTaskId { get; set; }
        public string HazardsId { get; set; }
        public string SafeguardsId { get; set; }
        public string ProtectiveMeasures { get; set; }
        public string RiskMatrixFlag { get; set; }
        public string Comment { get; set; }
        public string RobotTaskID { get; set; }
        public string RiskMatrix_s { get; set; }
        public string RiskMatrix_E { get; set; }
        public string RiskMatrix_A { get; set; }
        public string RiskMatrix_R { get; set; }
        public string RiskMatrix_PLLcAT { get; set; }
        public string Verification_S { get; set; }
        public string Verification_E { get; set; }
        public string Verification_A { get; set; }
        public string Verification { get; set; }
        public string Validation { get; set; }
        public string Chklevel { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentName { get; set; }
        public byte[] DocumentData { get; set; }

    }
}
