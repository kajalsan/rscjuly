﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RSCJuly.RobotTask
{

    [Table("UploadDocuments")]
    public  class UploadDocument : AuditedEntity
    {
        public int RobotTaskId { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentName { get; set; }
        public string DocumentType { get; set; }
        public byte[] DocumentData { get; set; }
    }
}
