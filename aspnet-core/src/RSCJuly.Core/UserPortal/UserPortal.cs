﻿using Abp.Domain.Entities.Auditing;
using RSCJuly.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RSCJuly.UserPortal
{
    [Table("UserPortal")]
    public class UserPortal : FullAuditedEntity
    {
        public const int MaxNameLength = 100;

        public const int MaxpasswordLength = 50;

        [ForeignKey(nameof(Company))]
        public int CompanyId { get; set; }
        public string FullName { get; set; }

        [StringLength(MaxNameLength)]
        public string EmailAddress { get; set; }
        public string Permission { get; set; }

        [StringLength(MaxpasswordLength)]
        public string Password { get; set; }

        [StringLength(MaxpasswordLength)]
        public string ConfirmPassword { get; set; }

        [ForeignKey(nameof(Locations))]
        public int LocationId { get; set; }

        [ForeignKey(nameof(User))]
        public int UserId { get; set; }

        
        


    }
}
