﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RSCJuly.Signup
{
    [Table("Signup")]
    public class Signup : FullAuditedEntity
    {
        public const int MaxNameLength = 100;

        public const int MaxpasswordLength = 50;

        [StringLength(MaxNameLength)]
        public string EmailID { get; set; }

        [StringLength(MaxpasswordLength)]
        public string Password { get; set; }

        //public int signupId { get; set; }

        //public virtual ICollection<Company.Company> Company { get; set; }

    }
}
