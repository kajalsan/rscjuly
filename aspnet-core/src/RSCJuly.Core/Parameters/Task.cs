﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using RSCJuly.Company;

namespace RSCJuly.Parameters
{
    [Table("Tasks")]
    public class Task : FullAuditedEntity
    {
        [ForeignKey(nameof(Company))]
        public int CompanyId { get; set; }

        public const int MaxNameLength = 100;

        [StringLength(MaxNameLength)]
        public string TaskName { get; set; }
        public bool Rsc { get; set; }
        public bool Msc { get; set; }
        public bool Csc { get; set; }
        public bool Defaultcheck { get; set; }

    }
}
