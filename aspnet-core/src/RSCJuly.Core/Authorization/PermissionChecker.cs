﻿using Abp.Authorization;
using RSCJuly.Authorization.Roles;
using RSCJuly.Authorization.Users;

namespace RSCJuly.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
