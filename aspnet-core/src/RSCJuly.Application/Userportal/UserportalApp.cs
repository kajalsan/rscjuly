﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Zero.Configuration;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RSCJuly.Authorization;
using RSCJuly.Authorization.Users;
using RSCJuly.EmailService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.Userportal
{
   public class UserportalApp: RSCJulyAppServiceBase, IApplicationService, IUserportalApp
    {
        private readonly IRepository<UserPortal.UserPortal> _userportalRepository;
        private readonly LogInManager _logInManager;
        private readonly IAbpSession _abpSession;
        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly UserManager _userManager;
        private readonly Emailer _emailer;


        public UserportalApp(IRepository<UserPortal.UserPortal> userportalRepository,
            LogInManager logInManager,IAbpSession abpSession,
            UserRegistrationManager userRegistrationManager, UserManager userManager, Emailer emailer
            )
        {
            _userportalRepository = userportalRepository;
            _abpSession = abpSession;
            _logInManager = logInManager;
            _userRegistrationManager = userRegistrationManager;

            _userManager = userManager;
            _emailer = emailer;

        }


      //  public async Task<UserPortal.UserPortal> CreateUser(UserPortal.UserPortal input)
      //  {

        //    var user = await _userRegistrationManager.createUserportal(

        //   input.FullName,
        //   input.EmailAddress,
        //   input.Password,

        //    true // Assumed email address is always confirmed. Change this if you want to implement email confirmation.
        //   );
          
        //    var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);


        //    return new userportalOutput
        //    {
               
        //        CanLogin = user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin)

        //    };
        //}


        public async Task<int> Create(UserPortal.UserPortal entity)
        {

            var com = _userportalRepository.FirstOrDefault(x => x.FullName == entity.FullName);
            if (com != null)
            {
                throw new UserFriendlyException(" Already exists");
            }
            else
            {
                return await _userportalRepository.InsertAndGetIdAsync(entity);
            }

          
        }

        public async Task<UserPortal.UserPortal> GetById(int UserportalId)
        {
            var query = _userportalRepository.GetAll()
                .Where(p => p.Id == UserportalId);

            var comp = await query.FirstOrDefaultAsync();
            return comp;
        }

        public int GetCountById()
        {
            var query = _userportalRepository.GetAll().Count();

            if (query > 0)
                return query;
            else
                return 0;
        }

        public List<UserPortal.UserPortal> GetAll()
        {
            var query = _userportalRepository.GetAll();
            //.OrderByDescending(x => x.Id)

            List<UserPortal.UserPortal> CompData = new List<UserPortal.UserPortal>();
            foreach (var item in query)
            {
                CompData.Add(new UserPortal.UserPortal
                {
                    Id = item.Id,
                   CompanyId = item.CompanyId,
                    FullName = item.FullName,
                    EmailAddress = item.EmailAddress,
                    Permission = item.Permission,
                    LocationId = item.LocationId,
                    UserId = item.UserId,
                });
            }
            return CompData;
        }


        public void Update(int userportalId, UserPortal.UserPortal entity)
        {
            var up = _userportalRepository.FirstOrDefault(x => x.Id == userportalId);
            if (up == null)
            {
                throw new UserFriendlyException("No  User data found");
            }
            else
            {
                //product.Id = entity.Id;
               // up.Id = entity.Id;
                up.FullName = entity.FullName;
                up.EmailAddress = entity.EmailAddress;
                up.Permission = entity.Permission;


                _userportalRepository.UpdateAsync(up);

            }
        }

        public void Updatepass(int userportalId, UserPortal.UserPortal entity)
        {
            var up = _userportalRepository.FirstOrDefault(x => x.Id == userportalId);
            if (up == null)
            {
                throw new UserFriendlyException("No  User data found");
            }
            else
            {
                
                up.Password = entity.Password;
                up.ConfirmPassword = entity.ConfirmPassword;
              
                _userportalRepository.UpdateAsync(up);

            }
        }


        public void Delete(int id)
        {
            var pm = _userportalRepository.FirstOrDefault(x => x.Id == id);
            if (pm == null)
            {
                throw new UserFriendlyException("No User data found");
            }
            else
            {
                _userportalRepository.Delete(pm);
            }
        }



    }
}
