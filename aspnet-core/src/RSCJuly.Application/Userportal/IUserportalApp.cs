﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.Userportal
{
  public  interface IUserportalApp: IApplicationService
    {

        List<UserPortal.UserPortal> GetAll();
        Task<int> Create(UserPortal.UserPortal entity);
            
     }
}
