﻿

using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RSCJuly.CompanyService
{
    public class CompanyApp : ApplicationService, ICompanyApp
    {
        private readonly IRepository<Company.Company> _ComRepository;

        public CompanyApp(IRepository<Company.Company> CompRepository)
        {
            _ComRepository = CompRepository;
        }

        public async Task<int> Create(Company.Company entity)
        {

            var com = _ComRepository.FirstOrDefault(x => x.Id == entity.Id);
            if (com != null)
            {
                throw new UserFriendlyException(" Already exists");
            }
            else
            {
                return await _ComRepository.InsertAndGetIdAsync(entity);
            }


        }
        public List<Company.Company> GetAll()
        {
            var query = _ComRepository.GetAll().OrderByDescending(x => x.Id); ;

            List<Company.Company> CompData = new List<Company.Company>();
            foreach (var item in query)
            {
                CompData.Add(new Company.Company
                {
                    Id = item.Id,
                    CompanyName = item.CompanyName,
                    Phoneno = item.Phoneno,
                });
            }
            return CompData;
        }


        public async Task<Company.Company> GetcompanyForEdit(int CompanyId)
        {
            var query = _ComRepository.GetAll()
                .Where(p => p.Id == CompanyId);

            var comp = await query.FirstOrDefaultAsync();
            return ObjectMapper.Map<Company.Company>(comp);
        }


        public void Update(Company.Company entity, int CompanyId)
        {
            var comp = _ComRepository.FirstOrDefault(x => x.Id == CompanyId);
            if (comp == null)
            {
                throw new UserFriendlyException("No Entry found");
            }
            else
            {
              
                comp.CompanyName = entity.CompanyName;
                comp.Phoneno = entity.Phoneno;
              

                _ComRepository.UpdateAsync(comp);

            }

        }

        public void UpdateAddress(int CompanyId,Company.Company entity)
        {
            var comp = _ComRepository.FirstOrDefault(x => x.Id == CompanyId);
            if (comp == null)
            {
                throw new UserFriendlyException("No Entry found");
            }
            else
            { comp.Address = entity.Address;
                _ComRepository.UpdateAsync(comp);
            }

        }


        public List<Company.Company> GetByCustomerId(int Id)
        {
            var item = _ComRepository.FirstOrDefault(x => x.Id == Id);

            if (item != null)
            {
                List<Company.Company> CompData = new List<Company.Company>();
                CompData.Add(new Company.Company
                {
                    Id = item.Id,
                    CompanyName = item.CompanyName,
                    Phoneno = item.Phoneno,
                });
                return CompData;
            }
            else
            {
                return null;
            }
        }


        //public void Updatesubscription(int CompanysId, string subscriptiontype)
        //{
        //    var comp = _ComRepository.FirstOrDefault(x => x.Id == CompanysId);
        //    if (comp == null)
        //    {
        //        throw new UserFriendlyException("No Entry found");
        //    }

        //    else
        //    {
        //        comp.subscriptiontype = subscriptiontype;

        //        _ComRepository.UpdateAsync(comp);
        //    }
        //}

        //public void Updatecompdata(int CompanysId, Company.Company entity)
        //{
        //    var comp = _ComRepository.FirstOrDefault(x => x.Id == CompanysId);
        //    if (comp == null)
        //    {
        //        throw new UserFriendlyException("No Entry found");
        //    }

        //    else
        //    {

        //        comp.country = entity.country;
        //        comp.state = entity.state;
        //        comp.city = entity.city;
        //        comp.zip = entity.zip;

        //        _ComRepository.UpdateAsync(comp);

        //    }

        //}


    }
}