﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.CompanyService
{
    public interface ICompanyApp : IApplicationService
    {
        Task<int> Create(Company.Company entity);
        List<Company.Company> GetAll();

        void Update(Company.Company entity, int CompanysId);
        //void Updatesubscription(int CompanysId, string subscriptiontype);
        //void Updatecompdata(int CompanysId, Company.Company entity);
        Task<Company.Company> GetcompanyForEdit(int CompanysId);


        void UpdateAddress(int CompanyId, Company.Company entity);

    }
}
