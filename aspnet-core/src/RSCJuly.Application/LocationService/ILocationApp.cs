﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.LocationService
{
   
 public interface ILocationApp : IApplicationService
    {
        Task<int> Create(Locations.Locations entity);
        List<Locations.Locations> GetAll(int CompanyId);

        void Update(int CompanysId,Locations.Locations entity);
      //  Task<Locations.Locations> GetcompanyForEdit(int CompanysId);

    }
}
