﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.LocationService
{
    public class LocationApp : ApplicationService, ILocationApp
    {
        private readonly IRepository<Locations.Locations> _LocRepository;

        public LocationApp(IRepository<Locations.Locations> LocRepository)
        {
            _LocRepository = LocRepository;
        }

        public async Task<int> Create(Locations.Locations entity)
        {

            var loc = _LocRepository.FirstOrDefault(x => x.Id == entity.Id);
            if (loc != null)
            {
                throw new UserFriendlyException(" Already exists");
            }
            else
            {
                return await _LocRepository.InsertAndGetIdAsync(entity);
            }


        }
        public List<Locations.Locations> GetAll(int CompanyId)
        {
            var query = _LocRepository.GetAll().OrderByDescending(x => x.CompanyId); ;

            List<Locations.Locations> locData = new List<Locations.Locations>();
            foreach (var item in query)
            {
                locData.Add(new Locations.Locations
                {
                    Id = item.Id,
                    CompanyId = item.CompanyId,
                    LocationName = item.LocationName,
                    country = item.country,
                    state = item.state,
                    city = item.city,
                    Address = item.Address,
                    zip = item.zip

                });
            }
            return locData;
        }

        public void Update(int CompanysId, Locations.Locations entity)
        {
            var loc = _LocRepository.FirstOrDefault(x => x.Id == CompanysId);
            if (loc == null)
            {
                throw new UserFriendlyException("No Entry found");
            }

            else
            {

                loc.country = entity.country;
                loc.state = entity.state;
                loc.city = entity.city;
                loc.zip = entity.zip;

                _LocRepository.UpdateAsync(loc);

            }

        }
    }
}
