﻿using Abp.Application.Services.Dto;

namespace RSCJuly.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

