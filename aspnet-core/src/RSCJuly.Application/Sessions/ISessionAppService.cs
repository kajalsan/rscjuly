﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RSCJuly.Sessions.Dto;

namespace RSCJuly.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
