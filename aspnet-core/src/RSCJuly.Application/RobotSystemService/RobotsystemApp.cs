﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using RSCJuly.RobotSystem;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.RobotSystemService
{
    public class RobotTaskApp : ApplicationService, IRobotTaskApp
    {
        private readonly IRepository<RobotSystem.RobotSystem> _robotSystemRepository;
        public RobotTaskApp(IRepository<RobotSystem.RobotSystem> robotSystemRepository)
        {
            _robotSystemRepository = robotSystemRepository;
        }

        public int Create(RobotSystem.RobotSystem entity)
        {
            return  _robotSystemRepository.InsertAndGetId(entity);
             

        }

        public List<RobotSystem.RobotSystem> getById(int id)
        {
            var query = _robotSystemRepository.GetAllList(x => x.Id == id);
            if (query == null)
            {
                return null;
            }
            else
            {
                return query;
            }
        }


        public List<RobotSystem.RobotSystem> getBycompanyid()
        {
            var query = _robotSystemRepository.GetAllList();
            if (query == null)
            {
                return null;
            }
            else
            {
                return query;
            }
        }
        public void deleteRobot(int id)
        {
            var query = _robotSystemRepository.FirstOrDefault(x => x.Id == id);
            if (query == null)
            {

            }
            else
            {
                _robotSystemRepository.Delete(query);

            }
        }



        public void updateRobot(RobotSystem.RobotSystem entity)
        {
            var query = _robotSystemRepository.FirstOrDefault(x => x.Id == entity.Id);
            if (query == null)
            {

            }
            else
            {
                query.RobotName = entity.RobotName;
                query.Asset = entity.Asset;
                query.ReasonRevision = entity.ReasonRevision;
                query.RiskAssessmentValidator = entity.RiskAssessmentValidator;
                query.LastRevision = entity.LastRevision;
                query.BuildingLocation = entity.BuildingLocation;
                query.IntegratorManufacturer = entity.IntegratorManufacturer;
                query.DateOrigin = entity.DateOrigin;
                query.ProjectManager = entity.ProjectManager;

                _robotSystemRepository.Update(query);

            }
        }

    }
}
