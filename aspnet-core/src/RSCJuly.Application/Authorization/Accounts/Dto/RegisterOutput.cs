﻿namespace RSCJuly.Authorization.Accounts.Dto
{
    public class RegisterOutput
    {

       public int UserId { get; set; }

        public bool CanLogin { get; set; }
    }
}
