﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RSCJuly.Authorization.Accounts.Dto;

namespace RSCJuly.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
        RegisterInput update(long UserId, RegisterInput input);
    }
}
