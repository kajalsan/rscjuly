using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.UI;
using Abp.Zero.Configuration;
using Castle.DynamicProxy.Internal;
using Microsoft.Extensions.Options;
using RSCJuly.Authorization.Accounts.Dto;
using RSCJuly.Authorization.Users;
using RSCJuly.EmailService;

namespace RSCJuly.Authorization.Accounts
{
    public class AccountAppService : RSCJulyAppServiceBase, IAccountAppService
    {
        // from: http://regexlib.com/REDetails.aspx?regexp_id=1923
        public const string PasswordRegex = "(?=^.{8,}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s)[0-9a-zA-Z!@#$%^&*()]*$";

        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly UserManager _userManager;
        private readonly Emailer _emailer;
      

        public AccountAppService(
            UserRegistrationManager userRegistrationManager, UserManager userManager, Emailer emailer)
        {
            _userRegistrationManager = userRegistrationManager;
           
                _userManager = userManager;
                _emailer = emailer;

        }

        public async Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input)
        {
            var tenant = await TenantManager.FindByTenancyNameAsync(input.TenancyName);
            if (tenant == null)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.NotFound);
            }

            if (!tenant.IsActive)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.InActive);
            }

            return new IsTenantAvailableOutput(TenantAvailabilityState.Available, tenant.Id);
        }

        public async Task<RegisterOutput> Register(RegisterInput input)
         {

                var user = await _userRegistrationManager.RegisterAsync(
               
                input.Name,
                input.Surname,
                input.EmailAddress,
                input.UserName,
                input.Password,
                //input.ConfirmPassword,
              
                true // Assumed email address is always confirmed. Change this if you want to implement email confirmation.
               );
            input.RoleNames = "ADMIN";

           var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);

         
            return new RegisterOutput
            {
                UserId = (int)user.Id,
                CanLogin = user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin)

            };
        }


        public RegisterInput GetbyId(long UserId)
        {
            var user = UserManager.GetUserById(UserId);
            return new RegisterInput
            {
                Name = user.Name,
                Surname = user.Surname,
                UserName = user.UserName,
                EmailAddress = user.EmailAddress,
                Password = user.Password,
                //ConfirmPassword = user.con
                // CanLogin = user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin)
            };
        }

        public RegisterInput update(long UserId, RegisterInput input)
        {
           
            var reg = UserManager.GetUserById(UserId);

            reg.Name = input.Name;
            reg.Surname = input.Surname;
            reg.UserName = input.UserName;
            reg.EmailAddress = input.EmailAddress;
            reg.Password = input.Password;
           // input.ConfirmPassword = user.confirm

            return input;

        }

        public async Task SendPasswordResetCode(SendPasswordResetCodeInput input)
        {
           var vser = await GetUserByChecking(input.EmailAddress);

           _emailer.forgetpassEmail(input.EmailAddress);

            throw new UserFriendlyException(L("EmailSentSuccesfully"));

        }

        public async Task<User> SendResetID(SendPasswordResetCodeInput input)
        {
            var user = await GetUserByChecking(input.EmailAddress);
            return user;

        }
        private async Task<User> GetUserByChecking(string inputEmailAddress)
        {

            //var user1 = await UserManager.FirstOrDefaultAsync(x => x.EmailAddress == "vilasmuthal@gmail.com");

            var user = await UserManager.FindByEmailAsync(inputEmailAddress);
            if (user == null)
            {
                throw new UserFriendlyException(L("This is Not a Registered EmailAddress"));
            }

            return user;
        }
 
    }
}
