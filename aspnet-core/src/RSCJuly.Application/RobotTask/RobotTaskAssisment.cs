﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RSCJuly.Configuration;
using RSCJuly.Parameters.Dto;
using RSCJuly.RobotTask.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.RobotTask
{
    public class RobotTaskAssisment : ApplicationService, IRobotTaskAssisment
    {

        private readonly IRepository<RobotTask> _robotTaskRepository;
        private readonly IRepository<UploadDocument> _UploadDocumentRepository;

        private readonly IRepository<Parameters.Task> _taskRepository;
        private readonly IRepository<Parameters.Subtask> _subtaskRepository;
        private readonly IRepository<Parameters.Hazard> _hazardRepository;

        private readonly IRepository<Parameters.Safeguard> _SafeguardRepository;
        private readonly IRepository<Parameters.ProtectiveMeasure> _ProtectiveMeasureRepository;
        //private readonly IRepository<ParameterDto> _parameterDtoRepository;

        private readonly IConfigurationRoot _appConfiguration;

        //private readonly IRepository<Parameters.sa> _taskRepository;
        private IHostingEnvironment _hostingEnvironment;


        public RobotTaskAssisment(IRepository<RobotTask> robotTaskRepository,
            IRepository<Parameters.Task> taskRepository,
            IRepository<Parameters.Subtask> subtaskRepository,
            IRepository<Parameters.Hazard> hazardRepository,
            IRepository<Parameters.Safeguard> SafeguardRepository,
            IRepository<Parameters.ProtectiveMeasure> ProtectiveMeasureRepository,
            IHostingEnvironment hostingEnvironment,
            IHostingEnvironment env,
            IRepository<UploadDocument> UploadDocumentRepository

            )
        {
            _robotTaskRepository = robotTaskRepository;
            _taskRepository = taskRepository;
            _subtaskRepository = subtaskRepository;
            _hazardRepository = hazardRepository;
            _SafeguardRepository = SafeguardRepository;
            _ProtectiveMeasureRepository = ProtectiveMeasureRepository;
            _appConfiguration = AppConfigurations.Get(env.ContentRootPath);
            _hostingEnvironment = hostingEnvironment;
            _UploadDocumentRepository = UploadDocumentRepository;

        }


        public async Task<int> Create(RobotTask entity)
        {

            return await _robotTaskRepository.InsertAndGetIdAsync(entity);

        }



        public int uploaddocument(IFormFile fName, int id)
        {

            //int customerId = customerdocument.CustomerId;
            var file = fName;
            string folderName = "TaskAssismentDocument";
            string contentType = file.ContentType;
            string ext = Path.GetExtension(file.FileName);
            //var file = "file:///C:/Users/Lenovo/Downloads/;
            //string folderName = "Upload";
            string webRootPath = _hostingEnvironment.WebRootPath;
            //C:\828platform_dev\aspnet - core\src\Platform828.Web.Host\wwwroot\logo
            //string Paths1 ="/" + "assets" + "/"+"images"+"/"+"uploads";
            string newPath = Path.Combine(webRootPath, folderName);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            string Idname = Convert.ToString(1234);
            string newidpath = Path.Combine(newPath, Idname);
            if (!Directory.Exists(newidpath))
            {
                Directory.CreateDirectory(newidpath);
            }
            Guid DocImageName = new Guid();
            DocImageName = Guid.NewGuid();
            var DocumentPath = Path.GetFullPath(@"wwwroot\" + folderName + @"\" + Idname + @"\" + DocImageName + ext);
            long countF = file.Length;
            int countsF = Convert.ToInt32(countF);
            if (file.Length > 0)
            {
                //string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                //string fullPath = Path.Combine(newPath, fileName);
                using (var stream = new FileStream(DocumentPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }
            var docPathToSave = _appConfiguration["App:ServerRootAddress"] + folderName + "/" + Idname + "/" + DocImageName + ext;

            UploadDocument upload = new UploadDocument();
            upload.DocumentPath = docPathToSave;
            upload.DocumentType = contentType;
            upload.DocumentName = DocImageName + ext;
            upload.RobotTaskId = id;
            _UploadDocumentRepository.InsertAndGetId(upload);
            return 1;


        }
        public List<RobotTask> getById(int companyId)
        {
            var query = _robotTaskRepository.GetAllList(x => x.CompanyId == companyId);
            if (query == null)
            {
                return null;
            }
            else
            {
                return query;
            }
        }

        public List<UploadDocument> getByIdDcoument(int RobotTaskId)
        {
            var query = _UploadDocumentRepository.GetAllList(x => x.RobotTaskId == RobotTaskId);
            if (query == null)
            {
                return null;
            }
            else
            {
                return query;
            }
        }



        public List<ParameterDto> GetParameterByCompanyId(int companyId)
        {
            var query = _taskRepository.GetAllList(x => x.CompanyId == companyId);

            var query1 = _subtaskRepository.GetAllList(x => x.CompanyId == companyId);

            var query2 = _hazardRepository.GetAllList(x => x.CompanyId == companyId);


            List<ParameterDto> Parameter = new List<ParameterDto>();


            foreach (var task in query)
            {
                foreach (var subtask in query1)
                {
                    foreach (var hazard in query2)
                    {
                        Parameter.Add(new ParameterDto
                        {
                            TaskId = task.Id.ToString(),
                            TaskName = task.TaskName,
                            SubTaskId = subtask.Id.ToString(),
                            SubTaskName = subtask.SubtaskName,
                            HazardId = hazard.Id.ToString(),
                            HazardName = hazard.HazName

                        });
                        //return Parameter;
                        //return Parameter;
                    }
                }
                // return Parameter;
            }
            return Parameter;

        }



        public List<RobotTaskAssismentDto> GetRobotTaskAssisment(int RobotId)
        {

            try
            {
                var query = _robotTaskRepository.GetAllList(x => x.RobotTaskID == RobotId.ToString());

                if (query == null)
                {
                    return null;
                }
                else
                {
                    List<RobotTaskAssismentDto> TempTaskAssisment = new List<RobotTaskAssismentDto>();

                    string TempTask = null;
                    //string TempsubTask = null;
                    //string Temphazard = null;
                    //string Tempsafegaurd = null;
                    //string Tempprotectivemeasure = null;
                    string TempchkLevel = null;

                    foreach (var item in query)
                    {
                        var query3 = _taskRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.TaskId));
                        if (query3 == null)
                        {
                            return null;
                        }
                        else
                        {
                            TempTask = query3.TaskName;
                        }

                        //var query1 = _subtaskRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.SubTaskId));

                        //if (query1 == null)
                        //{

                        //}
                        //else
                        //{
                        //    TempsubTask = query1.SubtaskName;
                        //}

                        //var query2 = _hazardRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.HazardsId));
                        //var query4 = _SafeguardRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.SafeguardsId));
                        //var query5 = _ProtectiveMeasureRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.ProtectiveMeasures));

                        if (item.Chklevel == null)
                        {
                            TempchkLevel = null;
                        }
                        else
                        {
                            TempchkLevel = item.Chklevel.ToString();
                        }
                        TempTaskAssisment.Add(new RobotTaskAssismentDto
                        {
                            TaskName = TempTask,
                            SubTaskName = item.SubTaskId,
                            HazardName = item.HazardsId,
                            SafeguardsName = item.SafeguardsId,
                            ProtectiveName = item.ProtectiveMeasures,
                            comment = item.Comment,
                            Id = item.Id,
                            RiskMatrix_s = item.RiskMatrix_s,
                            RiskMatrix_A = item.RiskMatrix_A,
                            RiskMatrix_E = item.RiskMatrix_E,
                            RiskMatrix_R = item.RiskMatrix_R,
                            RiskMatrix_PLLcAT = item.RiskMatrix_PLLcAT,
                            Chklevel = TempchkLevel,
                            documentPath=item.DocumentPath,

                             Verification = item.Verification,
                            Verification_A = item.Verification_A,
                            Verification_E = item.Verification_E,
                            Verification_S = item.Verification_S,
                            documentName=item.DocumentName


                            //HazardName= _hazardRepository.FirstOrDefault(x => x.Id =));

                        });
                    }
                    return TempTaskAssisment;
                }
            }

            catch (Exception ex)
            {
                Logger.Error("Not Found", ex);
                return null;
            }

        }

        public void deleteTaskAssisment(int id)
        {
            var query = _robotTaskRepository.FirstOrDefault(x => x.Id == id);
            if (query == null)
            {

            }
            else
            {
                _robotTaskRepository.Delete(query);

            }
        }



        public List<RobotTaskAssismentDto> GetRobotByIDTaskAssisment(int id)
        {
            var query = _robotTaskRepository.GetAllList(x => x.Id == id);


            List<RobotTaskAssismentDto> TempTaskAssisment = new List<RobotTaskAssismentDto>();
            string tempChkLevel = null;
            string TempTask = null;
            foreach (var item in query)
            {
                var query3 = _taskRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.TaskId));
                if (query3 == null)
                {
                    TempTask = null;
                }
                else
                {
                    TempTask = query3.TaskName.ToString();
                }

                //var query1 = _subtaskRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.SubTaskId));

                //if (query1 == null)
                //{

                //}
                //else
                //{
                //    TempsubTask = query1.SubtaskName;
                //}

                //var query2 = _hazardRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.HazardsId));
                //var query4 = _SafeguardRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.SafeguardsId));
                //var query5 = _ProtectiveMeasureRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.ProtectiveMeasures));

                if (item.Chklevel == null)
                {
                    tempChkLevel = null;
                }
                else
                {
                    tempChkLevel = item.Chklevel;
                }
                TempTaskAssisment.Add(new RobotTaskAssismentDto
                {
                    TaskName = TempTask,
                    SubTaskName = item.SubTaskId,
                    HazardName = item.HazardsId,
                    SafeguardsName = item.SafeguardsId,
                    ProtectiveName = item.ProtectiveMeasures,
                    comment = item.Comment,
                    Id = item.Id,
                    Chklevel = tempChkLevel,
                    RiskMatrix_s = item.RiskMatrix_s,
                    RiskMatrix_A = item.RiskMatrix_A,
                    RiskMatrix_E = item.RiskMatrix_E,
                    RiskMatrix_R = item.RiskMatrix_R,
                    RiskMatrix_PLLcAT = item.RiskMatrix_PLLcAT,
                    documentPath = item.DocumentPath,
                    Verification=item.Verification

                    //HazardName= _hazardRepository.FirstOrDefault(x => x.Id =));

                });
            }
            return TempTaskAssisment;

        }



        public int TaskAssismentUpdate(RobotTask entity)
        {
            var query = _robotTaskRepository.FirstOrDefault(x => x.Id == entity.Id);


            if (query == null)
            {
                return 0;
            }
            else
            {
                query.TaskId = entity.TaskId;
                query.SubTaskId = entity.SubTaskId;
                query.HazardsId = entity.HazardsId;
                query.SafeguardsId = entity.SafeguardsId;
                query.ProtectiveMeasures = entity.ProtectiveMeasures;
                query.Comment = entity.Comment;
                query.RiskMatrix_A = entity.RiskMatrix_A;
                query.RiskMatrix_E = entity.RiskMatrix_E;
                query.RiskMatrix_PLLcAT = entity.RiskMatrix_PLLcAT;
                query.RiskMatrix_R = entity.RiskMatrix_R;
                query.RiskMatrix_s = entity.RiskMatrix_s;
                query.Chklevel = entity.Chklevel;

                query.Verification_S = entity.Verification_S;
                query.Verification_E = entity.Verification_E;
                query.Verification_A = entity.Verification_A;
                query.Verification = entity.Verification;
                query.DocumentName = entity.DocumentName;




                _robotTaskRepository.Update(query);
                return entity.Id;

            }

        }


        public List<CopyTask> GetAllOldTask(int companyId)
        {
            var RootQuery = _robotTaskRepository.GetAllList(x => x.CompanyId == companyId);
            var query = _taskRepository.GetAll().OrderByDescending(x => x.Id);

            if (RootQuery != null)
            {
                List<CopyTask> TasklistData = new List<CopyTask>();
                foreach (var item in RootQuery)
                {
                    TasklistData.Add(new CopyTask
                    {
                        Id = item.Id,
                        TaskName = _taskRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.TaskId)).TaskName,
                        CompanyId = item.CompanyId,
                        TaskId = item.TaskId

                    });
                }
                return TasklistData;
            }
            else
            {
                return null;
            }
        }

        public List<RobotTaskAssismentDto> GetRobotByIdTask(int Id)
        {
            var query = _robotTaskRepository.GetAllList(x => x.Id == Id);


            List<RobotTaskAssismentDto> TempTaskAssisment = new List<RobotTaskAssismentDto>();

            string TempTask = null;
            string TempsubTask = null;
            string Temphazard = null;
            string Tempsafegaurd = null;
            string Tempprotectivemeasure = null;
            //string TempsubTask = null;

            foreach (var item in query)
            {
                var query3 = _taskRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.TaskId));
                if (query3 == null)
                {

                }
                else
                {
                    TempTask = query3.TaskName;
                }

                var query1 = _subtaskRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.SubTaskId));

                if (query1 == null)
                {

                }
                else
                {
                    TempsubTask = query1.SubtaskName;
                }

                var query2 = _hazardRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.HazardsId));
                var query4 = _SafeguardRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.SafeguardsId));
                var query5 = _ProtectiveMeasureRepository.FirstOrDefault(x => x.Id == Convert.ToInt32(item.ProtectiveMeasures));


                TempTaskAssisment.Add(new RobotTaskAssismentDto
                {
                    TaskName = TempTask,
                    SubTaskName = TempsubTask,
                    HazardName = query2.HazName,
                    SafeguardsName = query4.SafeguardName,
                    ProtectiveName = query5.ProtectiveName,
                    comment = item.Comment,
                    Id = item.Id


                    //HazardName= _hazardRepository.FirstOrDefault(x => x.Id =));

                });
            }
            return TempTaskAssisment;

        }
        public int verificationUpdate(verificationupdate entity)
        {
            var query = _robotTaskRepository.FirstOrDefault(x => x.Id == entity.Id);


            if (query == null)
            {
                return 0;
            }
            else
            {
                //query.TaskId = entity.TaskId;
                //query.SubTaskId = entity.SubTaskId;
                //query.HazardsId = entity.HazardsId;
                //query.SafeguardsId = entity.SafeguardsId;
                //query.ProtectiveMeasures = entity.ProtectiveMeasures;
                //query.Comment = entity.Comment;
                //query.RiskMatrix_A = entity.RiskMatrix_A;
                //query.RiskMatrix_E = entity.RiskMatrix_E;
                //query.RiskMatrix_PLLcAT = entity.RiskMatrix_PLLcAT;ffff
                //query.RiskMatrix_R = entity.RiskMatrix_R;fv
                //query.RiskMatrix_s = entity.RiskMatrix_s;
                //query.Chklevel = entity.Chklevel;

                query.Verification_S = entity.Verification_S;
                query.Verification_E = entity.Verification_E;
                query.Verification_A = entity.Verification_A;
                query.Verification = entity.Verification;
                query.DocumentName = entity.documentName;

                _robotTaskRepository.Update(query);
                return entity.Id;

            }

        }
    }

}
