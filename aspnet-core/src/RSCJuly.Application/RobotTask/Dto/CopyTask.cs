﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSCJuly.RobotTask.Dto
{
    public class CopyTask
    {
        public int CompanyId { get; set; }
        public int Id { get; set; }

        public string TaskName { get; set; }
        public string TaskId { get; set; }
    }
}
