﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSCJuly.RobotTask.Dto
{
   public class RobotTaskAssismentDto
    {
        public int Id { get; set; }

        public int companyId { get; set; }
        public string TaskName { get; set; }

        public string TaskId { get; set; }

        public string SubTaskName { get; set; }

        public string SubTaskId { get; set; }

        public string HazardName { get; set; }

        public string HazardId { get; set; }
        public string ProtectiveName { get; set; }
        public string ProtectiveId { get; set; }

        public string SafeguardsName { get; set; }

        public string SafeguardsId { get; set; }


        public string comment { get; set; }
 
        public string RiskMatrix_s { get; set; }
        public string RiskMatrix_E { get; set; }
        public string RiskMatrix_A { get; set; }
        public string RiskMatrix_R { get; set; }
        public string RiskMatrix_PLLcAT { get; set; }
        public string Verification_S { get; set; }
        public string Verification_E { get; set; }
        public string Verification_A { get; set; }

        public string Verification { get; set; }
        public string Validation { get; set; }
        public string Chklevel { get; set; }

        public string documentPath { get; set; }
        public string chkColor { get; set; }
        public string verificationchkColor { get; set; }

        public string documentName { get; set; }


    }
}
