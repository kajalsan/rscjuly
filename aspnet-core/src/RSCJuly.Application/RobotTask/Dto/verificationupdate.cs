﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSCJuly.RobotTask.Dto
{
    public class verificationupdate
    {
        public int Id { get; set; }

        public string Verification_S { get; set; }
        public string Verification_E { get; set; }
        public string Verification_A { get; set; }

        public string Verification { get; set; }
        public string Validation { get; set; }



        public string documentName { get; set; }


    }
}
