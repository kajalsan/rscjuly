﻿using RSCJuly.Parameters;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSCJuly.RobotTask.Dto
{
   public class ParameterDto
    {

        public string TaskName { get; set; }

        public string TaskId { get; set; }

        public string SubTaskName { get; set; }

        public string SubTaskId { get; set; }

        public string HazardName { get; set; }

        public string HazardId { get; set; }

        public string ProtectiveName { get; set; }
        public string ProtectiveId { get; set; }

        public string SafeguardsName { get; set; }

        public string SafeguardsId { get; set; }

    }
}
