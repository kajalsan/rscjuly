﻿using Abp.Application.Services;
using RSCJuly.MultiTenancy.Dto;

namespace RSCJuly.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

