﻿using Abp.Application.Services;
using System.Collections.Generic;
using RSCJuly.Parameters.Dto;

namespace RSCJuly.Parameters
{
    public interface IHazardService : IApplicationService
    {
        void Create(Hazard input);
        List<HazardDto> GetAll();
        void Update(Hazard entity);
        void Delete(int id);
    }
}