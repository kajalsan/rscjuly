﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using RSCJuly.Parameters.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.Parameters
{
    public class HazardService : ApplicationService, IHazardService
    {
        private readonly IRepository<Hazard> _hazRepository;
        private readonly IObjectMapper _objectMapper;

        public HazardService(IRepository<Hazard> hazRepository, IObjectMapper objectMapper)
        {
            _hazRepository = hazRepository;
            _objectMapper = objectMapper;
        }

        public void Create(Hazard entity)
        {
            var person = _hazRepository.FirstOrDefault(p => p.HazName == entity.HazName);
            if (person != null)
            {
                throw new UserFriendlyException("There is already a person with given email address");
            }
            _hazRepository.InsertAndGetId(entity);
        }
        public List<HazardDto> GetAll()
        {
            var query = _hazRepository.GetAll().OrderByDescending(x => x.Id); ;

            List<HazardDto> TasklistData = new List<HazardDto>();
            foreach (var item in query)
            {
                TasklistData.Add(new HazardDto
                {
                    Id = item.Id,
                    HazName = item.HazName,
                    Rsc = item.Rsc,
                    Msc = item.Msc,
                    Csc = item.Csc,
                    Defaultcheck = item.Defaultcheck,
                    CompanyId = item.CompanyId,
                    isReadonly = true,
                });
            }
            return TasklistData;
        }

        public void Update(Hazard entity)
        {
            var Ts = _hazRepository.FirstOrDefault(x => x.Id == entity.Id);
            if (Ts == null)
            {
                throw new UserFriendlyException("No  Task data found");
            }
            else
            {
                //product.Id = entity.Id;
                Ts.Id = entity.Id;
                Ts.HazName = entity.HazName;
                Ts.Rsc = entity.Rsc;
                Ts.Msc = entity.Msc;
                Ts.Csc = entity.Csc;
                Ts.Defaultcheck = entity.Defaultcheck;
                Ts.CompanyId = entity.CompanyId;

                _hazRepository.UpdateAsync(Ts);

            }
        }


        public async Task<Hazard> GetTaskForEdit(int id)
        {
            var query = _hazRepository.GetAll()
                 .Where(r => r.Id == id);

            var sgData = await query.FirstOrDefaultAsync();
            return ObjectMapper.Map<Hazard>(sgData);
        }

        public void Delete(int id)
        {
            var pm = _hazRepository.FirstOrDefault(x => x.Id == id);
            if (pm == null)
            {
                throw new UserFriendlyException("No safegaurd data found");
            }
            else
            {
                _hazRepository.Delete(pm);
            }
        }


    }
}