﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using RSCJuly.Company;
using Abp.Domain.Entities;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace RSCJuly.Parameters.Dto
{
    [AutoMapFrom(typeof(Hazard))]
    public class HazardDto : EntityDto<int>
    {
        public int CompanyId { get; set; }
        public string HazName { get; set; }
        public bool Rsc { get; set; }
        public bool Msc { get; set; }
        public bool Csc { get; set; }
        public bool Defaultcheck { get; set; }
        public bool isReadonly { get; set; }
    }
}
