﻿using Abp.Application.Services;
using System.Collections.Generic;
using RSCJuly.Parameters.Dto;

namespace RSCJuly.Parameters
{
    public interface ISubtaskService : IApplicationService
    {
        void Create(Subtask input);
        List<SubtaskDto> GetAll();
        void Update(Subtask entity);
        void Delete(int id);
    }
}