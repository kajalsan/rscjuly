﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSCJuly.Parameters.Dto;


namespace RSCJuly.Parameters
{
    public class SubtaskService : ApplicationService, ISubtaskService
    {
        private readonly IRepository<Subtask> _subtasksRepository;
        //private readonly IObjectMapper _objectMapper;

        public SubtaskService(IRepository<Subtask> subtaskRepository)
        {
            _subtasksRepository = subtaskRepository;
        }

        public void Create(Subtask entity)
        {
            var person = _subtasksRepository.FirstOrDefault(p => p.SubtaskName == entity.SubtaskName);
            if (person != null)
            {
                throw new UserFriendlyException("There is already a person with given email address");
            }
            else
            {
                _subtasksRepository.Insert(entity);
            }
        }

        public List<SubtaskDto> GetAll()
        {
            var query = _subtasksRepository.GetAll().OrderByDescending(x => x.Id); ;

            if (query != null)
            {
                List<SubtaskDto> TasklistData = new List<SubtaskDto>();
                foreach (var item in query)
                {
                    TasklistData.Add(new SubtaskDto
                    {
                        Id = item.Id,
                        SubtaskName = item.SubtaskName,
                        Rsc = item.Rsc,
                        Msc = item.Msc,
                        Csc = item.Csc,
                        Defaultcheck = item.Defaultcheck,
                        CompanyId = item.CompanyId,
                        isReadonly = true,
                        TaskId = item.TaskId
                    });
                }
                return TasklistData;
            }
            else
            {
                return null;
            }
        }

        public void Update(Subtask entity)
        {
            var Ts = _subtasksRepository.FirstOrDefault(x => x.Id == entity.Id);
            if (Ts == null)
            {
                throw new UserFriendlyException("No  Task data found");
            }
            else
            {
                //product.Id = entity.Id;
                Ts.Id = entity.Id;
                Ts.SubtaskName = entity.SubtaskName;
                Ts.Rsc = entity.Rsc;
                Ts.Msc = entity.Msc;
                Ts.Csc = entity.Csc;
                Ts.Defaultcheck = entity.Defaultcheck;

                _subtasksRepository.UpdateAsync(Ts);

            }
        }


        public async Task<Subtask> GetTaskForEdit(int id)
        {
            var query = _subtasksRepository.GetAll()
                 .Where(r => r.Id == id);

            var sgData = await query.FirstOrDefaultAsync();
            return ObjectMapper.Map<Subtask>(sgData);
        }

        public void Delete(int id)
        {
            var pm = _subtasksRepository.FirstOrDefault(x => x.Id == id);
            if (pm == null)
            {
                throw new UserFriendlyException("No safegaurd data found");
            }
            else
            {
                _subtasksRepository.Delete(pm);
            }
        }


    }
}