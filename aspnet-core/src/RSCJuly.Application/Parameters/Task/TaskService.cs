﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using RSCJuly.Parameters.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.Parameters
{
    public class TaskService : ApplicationService, ITaskService
    {
        private readonly IRepository<Task> _tasksRepository;
        private readonly IObjectMapper _objectMapper;

        public TaskService(IRepository<Task> taskRepository, IObjectMapper objectMapper)
        {
            _tasksRepository = taskRepository;
            _objectMapper = objectMapper;
        }

        public void Create(Task entity)
        {
            var person = _tasksRepository.FirstOrDefault(p => p.TaskName == entity.TaskName);
            if (person != null)
            {
                throw new UserFriendlyException("There is already a person with given email address");
            }
            _tasksRepository.InsertAndGetId(entity);
        }

        public List<TaskDto> GetAll()
        {
            var query = _tasksRepository.GetAll().OrderByDescending(x => x.Id); ;

            if (query != null)
            {
                List<TaskDto> TasklistData = new List<TaskDto>();
                foreach (var item in query)
                {
                    TasklistData.Add(new TaskDto
                    {
                        Id = item.Id,
                        TaskName = item.TaskName,
                        Rsc = item.Rsc,
                        Msc = item.Msc,
                        Csc = item.Csc,
                        Defaultcheck = item.Defaultcheck,
                        CompanyId = item.CompanyId,
                        isReadonly = true
                    });
                }
                return TasklistData;
            }
            else
            {
                return null;
            }
        }

        public void Update(Task entity)
        {
            var Ts = _tasksRepository.FirstOrDefault(x => x.Id == entity.Id);
            if (Ts == null)
            {
                throw new UserFriendlyException("No  Task data found");
            }
            else
            {
                //product.Id = entity.Id;
                Ts.Id = entity.Id;
                Ts.TaskName = entity.TaskName;
                Ts.Rsc = entity.Rsc;
                Ts.Msc = entity.Msc;
                Ts.Csc = entity.Csc;
                Ts.Defaultcheck = entity.Defaultcheck;
                Ts.CompanyId = entity.CompanyId;
                //Ts.Userid = entity.Userid;

                _tasksRepository.UpdateAsync(Ts);

            }
        }


        public async Task<Task> GetTaskForEdit(int id)
        {
            var query = _tasksRepository.GetAll()
                 .Where(r => r.Id == id);

            var sgData = await query.FirstOrDefaultAsync();
            return ObjectMapper.Map<Task>(sgData);
        }

        public void Delete(int id)
        {
            var pm = _tasksRepository.FirstOrDefault(x => x.Id == id);
            if (pm == null)
            {
                throw new UserFriendlyException("No safegaurd data found");
            }
            else
            {
                _tasksRepository.Delete(pm);
            }
        }


    }
}