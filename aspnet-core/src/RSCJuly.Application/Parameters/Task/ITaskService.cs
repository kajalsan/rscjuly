﻿using Abp.Application.Services;
using RSCJuly.Parameters.Dto;
using System.Collections.Generic;

namespace RSCJuly.Parameters
{
    public interface ITaskService : IApplicationService
    {
        void Create(Task input);
        List<TaskDto> GetAll();
        void Update(Task entity);
        void Delete(int id);

    }
}