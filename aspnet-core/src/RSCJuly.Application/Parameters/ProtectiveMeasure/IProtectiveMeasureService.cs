﻿using Abp.Application.Services;
using System.Collections.Generic;
using RSCJuly.Parameters.Dto;

namespace RSCJuly.Parameters
{
    public interface IProtectiveMeasureService : IApplicationService
    {
        void Create(ProtectiveMeasure input);
        List<ProtectiveMeasureDto> GetAll();
        void Update(ProtectiveMeasure entity);
        void Delete(int id);
    }
}