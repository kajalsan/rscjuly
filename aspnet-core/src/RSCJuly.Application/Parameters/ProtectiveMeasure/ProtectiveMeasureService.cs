﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSCJuly.Parameters.Dto;

namespace RSCJuly.Parameters
{
    public class ProtectiveMeasureService : ApplicationService , IProtectiveMeasureService
    {
        private readonly IRepository<ProtectiveMeasure> _protectiveRepository;
        private readonly IObjectMapper _objectMapper;

        public ProtectiveMeasureService(IRepository<ProtectiveMeasure> protectiveRepository, IObjectMapper objectMapper)
        {
            _protectiveRepository = protectiveRepository;
            _objectMapper = objectMapper;
        }

        public void Create(ProtectiveMeasure entity)
        {
            var person = _protectiveRepository.FirstOrDefault(p => p.ProtectiveName == entity.ProtectiveName);
            if (person != null)
            {
                throw new UserFriendlyException("There is already a person with given email address");
            }
            _protectiveRepository.InsertAndGetId(entity);
        }
        public List<ProtectiveMeasureDto> GetAll()
        {
            var query = _protectiveRepository.GetAll().OrderByDescending(x => x.Id); ;

            List<ProtectiveMeasureDto> TasklistData = new List<ProtectiveMeasureDto>();
            foreach (var item in query)
            {
                TasklistData.Add(new ProtectiveMeasureDto
                {
                    Id = item.Id,
                    ProtectiveName = item.ProtectiveName,
                    Rsc = item.Rsc,
                    Msc = item.Msc,
                    CompanyId = item.CompanyId,
                    Csc = item.Csc,
                    Defaultcheck = item.Defaultcheck,
                    isReadonly = true,
                });
            }
            return TasklistData;
        }

        public void Update(ProtectiveMeasure entity)
        {
            var Ts = _protectiveRepository.FirstOrDefault(x => x.Id == entity.Id);
            if (Ts == null)
            {
                throw new UserFriendlyException("No  Task data found");
            }
            else
            {
                //product.Id = entity.Id;
                Ts.Id = entity.Id;
                Ts.ProtectiveName = entity.ProtectiveName;
                Ts.Rsc = entity.Rsc;
                Ts.Msc = entity.Msc;
                Ts.Csc = entity.Csc;
                Ts.Defaultcheck = entity.Defaultcheck;
                Ts.CompanyId = entity.CompanyId;

                _protectiveRepository.UpdateAsync(Ts);

            }
        }


        public async Task<ProtectiveMeasure> GetTaskForEdit(int id)
        {
            var query = _protectiveRepository.GetAll()
                 .Where(r => r.Id == id);

            var sgData = await query.FirstOrDefaultAsync();
            return ObjectMapper.Map<ProtectiveMeasure>(sgData);
        }

        public void Delete(int id)
        {
            var pm = _protectiveRepository.FirstOrDefault(x => x.Id == id);
            if (pm == null)
            {
                throw new UserFriendlyException("No safegaurd data found");
            }
            else
            {
                _protectiveRepository.Delete(pm);
            }
        }
    }
}