﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSCJuly.Parameters.Dto;

namespace RSCJuly.Parameters
{
    public class SafeguardService : ApplicationService, ISafeguardService
    {
        private readonly IRepository<Safeguard> _safeguardRepository;
        private readonly IObjectMapper _objectMapper;

        public SafeguardService(IRepository<Safeguard> safeguardRepository, IObjectMapper objectMapper)
        {
            _safeguardRepository = safeguardRepository;
            _objectMapper = objectMapper;
        }

        public void Create(Safeguard entity)
        {
            var person = _safeguardRepository.FirstOrDefault(p => p.SafeguardName == entity.SafeguardName);
            if (person != null)
            {
                throw new UserFriendlyException("There is already a person with given email address");
            }
            _safeguardRepository.InsertAndGetId(entity);
        }

        public List<SafeguardDto> GetAll()
        {
            var query = _safeguardRepository.GetAll().OrderByDescending(x => x.Id); ;

            if (query != null)
            {
                List<SafeguardDto> TasklistData = new List<SafeguardDto>();
                foreach (var item in query)
                {
                    TasklistData.Add(new SafeguardDto
                    {
                        Id = item.Id,
                        SafeguardName = item.SafeguardName,
                        Rsc = item.Rsc,
                        Msc = item.Msc,
                        Csc = item.Csc,
                        Defaultcheck = item.Defaultcheck,
                        CompanyId = item.CompanyId,
                        isReadonly = true,
                    });
                }
                return TasklistData;
            }
            else
            {
                return null;
            }
        }

        public void Update(Safeguard entity)
        {
            var Ts = _safeguardRepository.FirstOrDefault(x => x.Id == entity.Id);
            if (Ts == null)
            {
                throw new UserFriendlyException("No  Task data found");
            }
            else
            {
                //product.Id = entity.Id;
                Ts.Id = entity.Id;
                Ts.SafeguardName = entity.SafeguardName;
                Ts.Rsc = entity.Rsc;
                Ts.Msc = entity.Msc;
                Ts.Csc = entity.Csc;
                Ts.Defaultcheck = entity.Defaultcheck;
                Ts.CompanyId = entity.CompanyId;
                //Ts.Userid = entity.Userid;

                _safeguardRepository.UpdateAsync(Ts);

            }
        }


        public async Task<Safeguard> GetTaskForEdit(int id)
        {
            var query = _safeguardRepository.GetAll()
                 .Where(r => r.Id == id);

            var sgData = await query.FirstOrDefaultAsync();
            return ObjectMapper.Map<Safeguard>(sgData);
        }

        public void Delete(int id)
        {
            var pm = _safeguardRepository.FirstOrDefault(x => x.Id == id);
            if (pm == null)
            {
                throw new UserFriendlyException("No safegaurd data found");
            }
            else
            {
                _safeguardRepository.Delete(pm);
            }
        }


    }
}