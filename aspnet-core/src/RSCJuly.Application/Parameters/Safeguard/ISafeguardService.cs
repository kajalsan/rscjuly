﻿using Abp.Application.Services;
using System.Collections.Generic;
using RSCJuly.Parameters.Dto;

namespace RSCJuly.Parameters
{
    public interface ISafeguardService : IApplicationService
    {
        void Create(Safeguard input);
        List<SafeguardDto> GetAll();
        void Update(Safeguard entity);
        void Delete(int id);

    }
}