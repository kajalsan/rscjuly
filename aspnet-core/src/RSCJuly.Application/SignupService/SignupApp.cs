﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.SignupService
{
    public class SignupApp: ApplicationService, ISignupApp
    {
        private readonly IRepository<Signup.Signup> _signupRepository;

        public SignupApp(IRepository<Signup.Signup> signuprepository)
        {
            _signupRepository = signuprepository;
           
        }

        public async Task<int> Create(Signup.Signup entity)
        {
            var SU = _signupRepository.FirstOrDefault(x => x.Id == entity.Id);
            if (SU != null)
            {
                throw new UserFriendlyException("Already User");
            }
            else
            {
                return await _signupRepository.InsertAndGetIdAsync(entity);
            }


        }

        public List<Signup.Signup> GetAll()
        {
           
                var query = _signupRepository.GetAll().OrderByDescending(x => x.Id);

                List<Signup.Signup> NewData = new List<Signup.Signup>();
                foreach (var item in query)
                {
                    NewData.Add(new Signup.Signup
                    {
                        Id = item.Id,
                        EmailID = item.EmailID,
                        Password = item.Password,
                       
                    });
                }

                return NewData;
          
        }



    }
}
