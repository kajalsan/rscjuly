﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSCJuly.SignupService
{
    public interface ISignupApp : IApplicationService
    {
        Task<int> Create(Signup.Signup entity);

        List<Signup.Signup> GetAll();


    }
}
