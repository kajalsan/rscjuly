﻿using System.Threading.Tasks;
using RSCJuly.Configuration.Dto;

namespace RSCJuly.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
