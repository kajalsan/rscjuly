﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using RSCJuly.Configuration.Dto;

namespace RSCJuly.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : RSCJulyAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
