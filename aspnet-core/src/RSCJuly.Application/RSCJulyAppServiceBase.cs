﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using RSCJuly.Authorization.Users;
using RSCJuly.MultiTenancy;
using RSCJuly.Authorization.Accounts.Dto;

namespace RSCJuly
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class RSCJulyAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        protected RSCJulyAppServiceBase()
        {
            LocalizationSourceName = RSCJulyConsts.LocalizationSourceName;
        }

        protected virtual async Task<User> GetCurrentUserAsync()
        {
            var user = await UserManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }

            return user;
        }

        //protected virtual async Task<User> GetbyId(int UserId)
        //{
        //    var query = UserManager.GetAll()
        //        .Where(p => p.Id == CompanysId);

        //    var comp = await query.FirstOrDefaultAsync();
        //    return ObjectMapper.Map<RegisterInput>(comp);

        //}

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
