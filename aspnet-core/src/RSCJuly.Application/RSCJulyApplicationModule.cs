﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using RSCJuly.Authorization;

namespace RSCJuly
{
    [DependsOn(
        typeof(RSCJulyCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class RSCJulyApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<RSCJulyAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(RSCJulyApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
