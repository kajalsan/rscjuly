using System.ComponentModel.DataAnnotations;

namespace RSCJuly.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}