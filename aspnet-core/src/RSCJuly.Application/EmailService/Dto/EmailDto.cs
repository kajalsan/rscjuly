﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSCJuly.EmailService.Dto
{
    public class EmailDto
    {
        public string toEmail { get; set; }
        public string mailSubject { get; set; }
        public string mailMessage { get; set; }

        public string UserId { get; set; }
    }
}
