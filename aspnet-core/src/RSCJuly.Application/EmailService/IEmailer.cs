﻿using Abp.Application.Services;
using RSCJuly.Authorization.Users;
using RSCJuly.EmailService.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSCJuly.EmailService
{
    public interface IEmailer : IApplicationService
    {
       void SendEmail(string emailToAddress);
        void forgetpassEmail(string emailToAddress);
    }
}