﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RSCJuly.RobotTask
{
    public class RobotTaskApp : ApplicationService, IRobotTaskApp
    {
        private readonly IRepository<RobotTask> _robotTaskRepository;

        public RobotTaskApp(
            IRepository<RobotTask> robotTaskRepository
            )
        {
            _robotTaskRepository = robotTaskRepository;
        }

        public async Task<RobotTask> Create(RobotTask entity)
        {
            return await _robotTaskRepository.InsertAsync(entity);

        }


        //public List<RobotTask> getById(int companyId)
        //{
        //    var query = _robotTaskRepository.GetAllList(x => x.CompanyId == companyId);
        //    if (query == null)
        //    {
        //        return null;
        //    }
        //    else
        //    {
        //        return query;
        //    }
        //}
    }
}
