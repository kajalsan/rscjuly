import { RSCJulyTemplatePage } from './app.po';

describe('RSCJuly App', function() {
  let page: RSCJulyTemplatePage;

  beforeEach(() => {
    page = new RSCJulyTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
