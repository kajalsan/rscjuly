import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccountRoutingModule } from './account-routing.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { AccountComponent } from './account.component';
import { LoginComponent } from './login/login.component';   
import { RegisterComponent } from './register/register.component';
import { AccountLanguagesComponent } from './layout/account-languages.component';
import { AccountHeaderComponent } from './layout/account-header.component';
import { AccountFooterComponent } from './layout/account-footer.component';

// tenants
import { TenantChangeComponent } from './tenant/tenant-change.component';
import { TenantChangeDialogComponent } from './tenant/tenant-change-dialog.component';
import { CompanyComponent } from './company/company.component';
import { SubscriptionPageComponent } from './subscription-page/subscription-page.component';
import { PaymentDetailsComponent } from './payment-details/payment-details.component';
import { AgmCoreModule } from '@agm/core';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ForgetpasswordformComponent } from './forgetpasswordform/forgetpasswordform.component';
import {PhoneMaskDirective} from './phone-mask.directive';
import {MaterialModule} from '../app/material.module';

@NgModule({
    imports: [
        
        CommonModule,
        FormsModule,
        MaterialModule,
        HttpClientModule,
        HttpClientJsonpModule,
        SharedModule,
        ServiceProxyModule,
        AccountRoutingModule,
        ModalModule.forChild(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDeBg1S6PN6R8CJkzCgv-qodYjEOJnBH1o',
            libraries: ['places']
          })
          
    ],
    // exports:[PhoneMaskDirective],
    declarations: [
        AccountComponent,
        LoginComponent,
        RegisterComponent,
        AccountLanguagesComponent,
        AccountHeaderComponent,
        AccountFooterComponent,
        PhoneMaskDirective,

        // tenant
        TenantChangeComponent,
        TenantChangeDialogComponent,
        CompanyComponent,
        SubscriptionPageComponent,
        PaymentDetailsComponent,
        ForgetPasswordComponent,
        ForgetpasswordformComponent,
    ],
    entryComponents: [
        // tenant
        TenantChangeDialogComponent
    ]
})
export class AccountModule {

}
