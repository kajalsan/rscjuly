import { Component, OnInit, Injector, NgZone, ViewChild, ElementRef } from '@angular/core';
import { CompanyAppServiceProxy, Company, AccountServiceProxy, RegisterOutput, RegisterInput, LocationAppServiceProxy } from '@shared/service-proxies/service-proxies';
import { result } from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-payment-details',
  templateUrl: './payment-details.component.html',
  styleUrls: ['./payment-details.component.css']
})
export class PaymentDetailsComponent extends AppComponentBase implements OnInit {
  model: Company = new Company();
  model1: RegisterInput = new RegisterInput();
  show = false;
  saving = false;
  value: any;
  users: string;
  selectedplan: string;
  companyId:any;
  Cname: string;
  pnumber: string;
  address: string;
  select_plan: string;
  latitude: number;
  longitude: number;
  zoom: number;
  // user = new User();

  
  
  @ViewChild('search')
  public searchElementRef: ElementRef;
  UserId: number;
  addresss: string;
  checkbox1: boolean;
  checkbox2: boolean;
  Compaddresss: string;
  result: void;

  constructor(
    injector: Injector,
    private router : Router,
    private _accountService: AccountServiceProxy,
   private authService: AppAuthService,
     private companysevice: CompanyAppServiceProxy,
     private mapsAPILoader: MapsAPILoader,
     private ngZone: NgZone,
     private locservice:LocationAppServiceProxy
  ) { 
    super(injector);
  }

  selectedValue:any;


  callme(e) {
    this.value = e.target.value
  }
 
 
  ngAfterViewInit()
  {
    this.findAdress();
  }

  findAdress(){
    this.mapsAPILoader.load().then(() => {
         let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
         autocomplete.addListener("place_changed", () => {
           this.ngZone.run(() => {
             let place: google.maps.places.PlaceResult = autocomplete.getPlace();
             this.address = place.formatted_address;
             this.latitude = place.geometry.location.lat();
             this.longitude = place.geometry.location.lng();
             this.zoom = 12;
           });
         });
       });
   }

  ngOnInit(): void {

    this.setDefaultValues();

    this.selectedplan = localStorage.getItem('selectPlan')
    console.log(this.selectedplan.toString());

    this.UserId = +localStorage.getItem("registerId")

    this.companyId = +localStorage.getItem('CompanyId')

    this.companysevice.getcompanyForEdit(this.companyId).subscribe((res)=>
    {

        this.model = res;
        //  this.Cname = res.companyName;
        //  this.pnumber = res.phoneno;
         this.Compaddresss = res.address
    })


  }

  toggleCheckbox(event) {
    event.target.id === 'checkbox1' ? (this.checkbox1 = true, this.checkbox2 = false)
      : (this.checkbox2 = true, this.checkbox1 = false);
  }

  setDefaultValues() {
    this.checkbox1 = true;
    this.checkbox2 = false;
  }

  // 
  changePricing(e) {
    this.value = e.target.value

    if (this.value == '0.00/-') {
      this.selectedplan = "Free Plan"
      this.users = '1 users'
      this.select_plan = ''
      this.model.subscriptiontype = 'free';
    }

    else if (this.value == '99.99/-')
    {
      this.selectedplan = "Monthly"
      this.users = '5 users/Location'
      this.select_plan = 'Monthly'
      this.model.subscriptiontype = 'Monthly';
    }

    else {
      this.selectedplan = "Annual Plan"
      this.users = '5 users/Location'
      this.select_plan = 'Annum'
      this.model.subscriptiontype = 'Annual';
    }
  }
 
  save() {

    this.Updatecomapany(result);

   // this.UpdatecomapanyAddress(result);

    //this.GetAddress(result);

    this.router.navigate(['account/login']);
  }


  Updatecomapany(data:any){

    // this.UserId = this.model1.userId;
    data.subscriptiontype = this.model.subscriptiontype;
    data = this.model;


    
    this.companysevice.update(this.companyId,data).subscribe((res)=>{

   
     
    //  this.notify.success(this.l('Update Successfully'));

         console.log(res);
    })

  }

  UpdatecomapanyAddress(data:any){

    // this.UserId = this.model1.userId;
    data = this.model;
    
    this.companysevice.updateAddress(this.companyId,data).subscribe((res)=>{
  //    this.notify.success(this.l('Update Address Successfully'));
        
         console.log(res);
    })

  }

 
   
  GetAddress(result)
  {
   
     result.id = this.model.id,
     result.address = this.model.address,
   

    this.locservice.create(result).subscribe((res)=>{

    })
  }


}
