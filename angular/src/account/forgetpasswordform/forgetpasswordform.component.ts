import { Component, OnInit, Output, EventEmitter, Injector } from '@angular/core';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';
import { RegisterInput, ResetPasswordDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-forgetpasswordform',
  templateUrl: './forgetpasswordform.component.html',
  styleUrls: ['./forgetpasswordform.component.css']
})
export class ForgetpasswordformComponent  implements OnInit {

  model: RegisterInput = new RegisterInput();
  passwordTextType: boolean;
  confirmpassTextType : boolean;
  // public isLoading = false;
  // public resetPasswordDto: ResetPasswordDto;
  // id: number;

  passwordFieldTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  confirmpasswordFieldTextType() {
    this.confirmpassTextType = !this.confirmpassTextType;
  }

  passwordValidationErrors: Partial<AbpValidationError>[] = [
    {
      name: 'pattern',
      localizationKey:
        'PasswordsMustBeAtLeast8CharactersContainLowercaseUppercaseNumber',
    },
  ];
  confirmPasswordValidationErrors: Partial<AbpValidationError>[] = [
    {
      name: 'validateEqual',
      localizationKey: 'PasswordsDoNotMatch',
    },
  ];

  @Output() onSave = new EventEmitter<any>();

  // constructor(
  //   injector: Injector,
  //   // private _userService: UserServiceProxy,
  //   // public bsModalRef: BsModalRef
  // ) {
  //   super(injector);
  // }

  ngOnInit(): void {
    // this.isLoading = true;
    // this.resetPasswordDto = new ResetPasswordDto();
    // this.resetPasswordDto.userId = this.id;
    // this.resetPasswordDto.newPassword = Math.random()
    //   .toString(36)
    //   .substr(2, 10);
    // this.isLoading = false;
  }


  // save(data:any){
  //   this.isLoading = true;
  //   this._userService
  //     .resetPassword(this.resetPasswordDto)
  //     .pipe(
  //       finalize(() => {
  //         this.isLoading = false;
  //       })
  //     )
  //     .subscribe(() => {
  //       this.notify.info('Password Reset');
  //       this.bsModalRef.hide();
  //     });
  // }
  }


