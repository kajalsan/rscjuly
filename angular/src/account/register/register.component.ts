import { Component, Injector, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import {
  AccountServiceProxy,
  RegisterInput,
  RegisterOutput,
  SignupAppServiceProxy,
  Signup,
  EmailerServiceProxy
} from '@shared/service-proxies/service-proxies';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';
import * as _ from 'lodash';

@Component({
  templateUrl: './register.component.html',
  animations: [accountModuleAnimation()],
  styleUrls: ['./register.component.css']
})
export class RegisterComponent extends AppComponentBase {
  model: RegisterInput = new RegisterInput();
  // model:Signup = new Signup();
  saving = false;
  ID: any;
  signupId: any;
  Login: boolean;
  Username: string;
  password: string;
  name: any;
  surname: any;
  emailAddress: any;
  passwordTextType: boolean;
  confirmpassTextType : boolean;
  checkedRolesMap: { [key: string]: boolean } = {};

  passwordFieldTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  confirmpasswordFieldTextType() {
    this.confirmpassTextType = !this.confirmpassTextType;
  }

  passwordValidationErrors: Partial<AbpValidationError>[] = [
    {
      name: 'pattern',
      localizationKey:
        'PasswordsMustBeAtLeast8CharactersContainLowercaseUppercaseNumber',
    },
  ];
  confirmPasswordValidationErrors: Partial<AbpValidationError>[] = [
    {
      name: 'validateEqual',
      localizationKey: 'PasswordsDoNotMatch',
    },
  ];

  @Output() onSave = new EventEmitter<any>();


  constructor(
    injector: Injector,
    private _accountService: AccountServiceProxy,
    private _signupService: SignupAppServiceProxy,
    private _router: Router,
    private authService: AppAuthService,
    private emailSender: EmailerServiceProxy
  ) {
    super(injector);
  }

  keytab(event){
  this.model.userName = this.model.emailAddress;
}

getCheckedRoles(): string[] {
  const roles: string[] = [];
  _.forEach(this.checkedRolesMap, function (value, key) {
    if (value) {
      roles.push(key);
    }
  });
  return roles;
}

  save(): void {
    this.saving = true;

    //this.model.roleNames = ['ADMIN'];    

    this.model.userName = this.model.emailAddress;

    this._accountService.register(this.model)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe((result: RegisterOutput) => {
       
      this.signupId = result.userId;

        this.Login = result.canLogin;

        console.log(this.signupId);

        //  if (!result.canLogin) {
        this.notify.success(this.l('SuccessfullyRegistered'));
        // this._router.navigate(['/login']);
        // this.emailSender.sendEmail(this.model.emailAddress).subscribe((res)=>{
        //   this.notify.success(this.l('Verfication mail send to your email Id'));
        // })
        localStorage.setItem('userId', this.signupId.toString());

        this._router.navigate(['/account/company/', this.signupId])

        return;
        // }

        //  Autheticate
          this.saving = true;
          this.authService.authenticateModel.userNameOrEmailAddress = this.model.userName;
          this.authService.authenticateModel.password = this.model.password;
          this.authService.authenticate(() => {
            this.saving = false;
          });
      });


    // this.Username = this.model.userName;
    this.password = this.model.password;
    // this.name = this.model.name;
    this.emailAddress = this.model.emailAddress;
    // this.surname = this.model.surname;
    // localStorage.setItem('uname', this.Username.toString());
    localStorage.setItem('password', this.password.toString());
    // localStorage.setItem('name', this.name.toString());
    // localStorage.setItem('surname', this.surname.toString());
    localStorage.setItem('emailAddress', this.emailAddress.toString());


  }

}