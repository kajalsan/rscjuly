import { Component, OnInit, Injector, ViewChild, ElementRef, NgZone} from '@angular/core';
import { Company, AccountServiceProxy, CompanyAppServiceProxy, Signup, LocationAppServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import { MapsAPILoader } from '@agm/core';
// import { AGMMouseEvent } from '@agm/core';


@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent extends AppComponentBase implements OnInit {

  model: Company = new Company();
  saving = false;
  registerId: any;

  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;

 @ViewChild('search')
  public searchElementRef: ElementRef;
 
  constructor(
    injector: Injector,
    private _accountService: AccountServiceProxy,
    private _comapnyService: CompanyAppServiceProxy,
    private _router: Router,
    private authService: AppAuthService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private locservice:LocationAppServiceProxy,

  ) {
    super(injector);
  }

  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";

  ngOnInit() {
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        // this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  ngAfterViewInit()
  {
    this.findAdress();
  }

  findAdress(){
    this.mapsAPILoader.load().then(() => {
         let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
         autocomplete.addListener("place_changed", () => {
           this.ngZone.run(() => {
             let place: google.maps.places.PlaceResult = autocomplete.getPlace();
             this.address = place.formatted_address;
             this.latitude = place.geometry.location.lat();
             this.longitude = place.geometry.location.lng();
             this.zoom = 12;
           });
         });
       });
   }

 save(): void {
    this.saving = true;

    console.log(this.address);

    this.model.address = this.address;

    this.model.userId = +localStorage.getItem('userId');

    this._comapnyService.create(this.model).pipe(

      finalize(() => {
        this.saving = false;
      })
    )
      .subscribe((Company) => {

        this.registerId = this.model.userId;
        // this.GetAddress(Company, this.model.address);
        this.notify.success(this.l('SuccessfullyRegistered'));
        localStorage.setItem('CompanyId', Company.toString());
        localStorage.setItem('registerId', this.registerId.toString());

        this._router.navigate(['/account/subscription/', this.registerId, Company])
        return;

      });
  }


  // GetAddress(CompanyId, data){

  //  // data = this.model.id;
  //  this.locservice.create(data).subscribe((res)=>{
  //   })

  // }


}
