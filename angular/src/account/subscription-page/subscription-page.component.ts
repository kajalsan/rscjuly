import { Component, OnInit, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Company, CompanyAppServiceProxy, AccountServiceProxy, RegisterInput, RegisterOutput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import { AppAuthService } from '@shared/auth/app-auth.service';

@Component({
  selector: 'app-subscription-page',
  templateUrl: './subscription-page.component.html',
  styleUrls: ['./subscription-page.component.css']
})
export class SubscriptionPageComponent extends AppComponentBase implements OnInit {

  model: Company = new Company();
  model1: RegisterInput = new RegisterInput();

  saving = false;
  value: any;
  companyId: number;
  registerId: number;
  LoginDetails: any;

  constructor(private router: Router,
    injector: Injector,
    private _accountService: AccountServiceProxy,
    private _comapnyService: CompanyAppServiceProxy,
    private authService: AppAuthService
  ) {
    super(injector);
  }

  ngOnInit(): void {
  }
  Selectplan(plan) {

    this.model.id = +localStorage.getItem('CompanyId');
    this.companyId = this.model.id;

    this.registerId = +localStorage.getItem('registerId');


    localStorage.setItem('selectPlan', plan.toString());
    this.router.navigate(['/account/paymentdetail/', this.registerId, this.companyId])

  }

 
}
