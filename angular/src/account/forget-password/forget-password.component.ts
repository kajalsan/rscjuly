import { Component, OnInit, Injector } from '@angular/core';
import { AccountServiceProxy, SignupAppServiceProxy, EmailerServiceProxy, SendPasswordResetCodeInput } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent extends AppComponentBase implements OnInit {

  model: SendPasswordResetCodeInput = new SendPasswordResetCodeInput();

  constructor(
    injector: Injector,
    private _accountService: AccountServiceProxy,
    private _signupService: SignupAppServiceProxy,
    private _router: Router,
    private authService: AppAuthService,
    private emailSender: EmailerServiceProxy
  ) {
    super(injector);
   }

  ngOnInit(): void {
  }

  save(data:any)
  {
     this._accountService.sendPasswordResetCode(this.model).subscribe((res)=>{
      //  this.emailSender.forgetpassEmail().subscribe((res)=>{
     //   this.notify.success(this.l('SuccessfullyRegistered'));
      //  })
     })
  }

}
