import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AccountComponent } from './account.component';
import { CompanyComponent } from './company/company.component';
import { SubscriptionPageComponent } from './subscription-page/subscription-page.component';
import { PaymentDetailsComponent } from './payment-details/payment-details.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ForgetpasswordformComponent } from './forgetpasswordform/forgetpasswordform.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AccountComponent,
                children: [
                    { path: 'login', component: LoginComponent },
                    { path: 'register', component: RegisterComponent },
                    {path:'forgetpass', component:ForgetPasswordComponent},
                    {path:'forgetpassform', component:ForgetpasswordformComponent},
                    { path: 'company/:SignupId', component: CompanyComponent },
                    { path: 'subscription/:registerId/:companyId', component: SubscriptionPageComponent },
                    { path: 'paymentdetail/:registerId/:companyId', component: PaymentDetailsComponent }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule { }
