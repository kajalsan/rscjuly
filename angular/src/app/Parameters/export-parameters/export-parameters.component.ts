import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HazardServiceServiceProxy, ProtectiveMeasureServiceServiceProxy, SafeguardServiceServiceProxy, SubtaskServiceServiceProxy, TaskServiceServiceProxy, TaskDto, SubtaskDto, HazardDto, SafeguardDto, ProtectiveMeasureDto } from '@shared/service-proxies/service-proxies';
import { MatSelectionList } from '@angular/material/list';
import * as xlsx from 'xlsx';
import { Router, ActivatedRoute } from '@angular/router';


const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  selector: 'app-export-parameters',
  templateUrl: './export-parameters.component.html',
  styleUrls: ['./export-parameters.component.css'],
  providers: [HazardServiceServiceProxy, ProtectiveMeasureServiceServiceProxy, SafeguardServiceServiceProxy, SubtaskServiceServiceProxy, TaskServiceServiceProxy]
})


export class ExportParametersComponent implements OnInit {

  TaskdData: TaskDto[] = [];
  SubtaskdData: SubtaskDto[] = [];
  HazardData: HazardDto[] = [];
  SafeguardData: SafeguardDto[] = [];
  ProtectiveData: ProtectiveMeasureDto[] = [];

  taskSelectedOptions: string[];
  subtaskSelectedOptions: string[];
  hazardSelectedOptions: string[];
  protectSelectedOptions: string[];
  safeguardSelectedOptions: string[];


  @ViewChild('task', { static: true }) private task: MatSelectionList;
  @ViewChild('subtask', { static: true }) private subtask: MatSelectionList;
  @ViewChild('hazard', { static: true }) private hazard: MatSelectionList;
  @ViewChild('protectM', { static: true }) private protectM: MatSelectionList;
  @ViewChild('safeguard', { static: true }) private safeguard: MatSelectionList;
  @ViewChild('task') parameterTask: ElementRef;

  taskLen: number;
  subtaskLen: number;
  hazardLen: number;
  protecLen: number;
  safeLen: number;

  taskCheckbox: boolean = false;
  subtaskCheckbox: boolean = false;
  hazardCheckbox: boolean = false;
  protectCheckbox: boolean = false;
  safeCheckbox: boolean = false;
  taskList: any = [];
  subtaskList: any = [];
  hazardList: any = [];
  protectList: any = [];
  safegaurdList: any = [];

  constructor(
    private hazservice: HazardServiceServiceProxy,
    private protectiveservice: ProtectiveMeasureServiceServiceProxy,
    private safeguardkservice: SafeguardServiceServiceProxy,
    private subtaskservice: SubtaskServiceServiceProxy,
    private taskservice: TaskServiceServiceProxy,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.getAllTask()
    this.getAllSubtask()
    this.getAllSafeguard()
    this.getAllProtective()
    this.getAllHazard()

  }

  getAllTask() {
    this.taskservice.getAll().subscribe(res => {
      this.TaskdData = res;
      this.taskLen = this.TaskdData.length
    })
  }

  getAllSubtask() {
    this.subtaskservice.getAll().subscribe(res => {
      this.SubtaskdData = res;
      this.subtaskLen = this.SubtaskdData.length
    })
  }

  getAllHazard() {
    this.hazservice.getAll().subscribe(res => {
      this.HazardData = res;
      this.hazardLen = this.HazardData.length;
    })
  }

  getAllProtective() {
    this.protectiveservice.getAll().subscribe(res => {
      this.ProtectiveData = res;
      this.protecLen = this.ProtectiveData.length
    })
  }

  getAllSafeguard() {
    this.safeguardkservice.getAll().subscribe(res => {
      this.SafeguardData = res;
      this.safeLen = this.SafeguardData.length
    })
  }

  selectAllTask(e) {
    if (e.checked == true) {
      this.task.selectAll();
      this.taskCheckbox = true
    }
    else if (e.checked == false) {
      this.task.deselectAll();
    }
  }

  selectAllSubTask(e) {
    if (e.checked == true) {
      this.subtask.selectAll();
      this.subtaskCheckbox = true
    }
    else if (e.checked == false) {
      this.subtask.deselectAll();
    }
  }

  selectAllHazard(e) {
    if (e.checked == true) {
      this.hazard.selectAll();
      this.hazardCheckbox = true;
    }
    else if (e.checked == false) {
      this.hazard.deselectAll();
    }
  }

  selectAllProtect(e) {
    if (e.checked == true) {
      this.protectM.selectAll();
      this.protectCheckbox = true;
    }
    if (e.checked == false) {
      this.protectM.deselectAll();
    }
  }

  selectAllSafeguard(e) {
    if (e.checked == true) {
      this.safeguard.selectAll();
      this.safeCheckbox = true;
    }
    else if (e.checked == false) {
      this.safeguard.deselectAll();
    }
  }

  onNgModelChangeTask(e) {
    this.taskSelectedOptions = JSON.parse(JSON.stringify(e).toString());
    console.log(this.taskSelectedOptions)
    console.log(this.TaskdData)
    if (this.taskSelectedOptions.length == this.taskLen) {
      this.taskCheckbox = true;
    }
  }

  onSelectionTask(e) {
    console.log(e.option.selected)
    if (e.option.selected == false) {
      this.taskCheckbox = false;
    }
  }

  onNgModelChangeSubtask(e) {
    this.subtaskSelectedOptions = JSON.parse(JSON.stringify(e));
    console.log(this.subtaskSelectedOptions)
    if (this.subtaskSelectedOptions.length == this.subtaskLen) {
      this.subtaskCheckbox = true;
    }
  }

  onSelectionSubtask(e) {
    console.log(e.option.selected)
    if (e.option.selected == false) {
      this.subtaskCheckbox = false;
    }
  }

  onNgModelChangeHazard(e) {
    this.hazardSelectedOptions = JSON.parse(JSON.stringify(e));
    console.log(this.hazardSelectedOptions)
    if (this.hazardSelectedOptions.length == this.hazardLen) {
      this.hazardCheckbox = true;
    }
  }

  onSelectionHazard(e) {
    console.log(e.option.selected)
    if (e.option.selected == false) {
      this.hazardCheckbox = false;
    }
  }

  onNgModelChangeProtect(e) {
    this.protectSelectedOptions = JSON.parse(JSON.stringify(e));
    if (this.protectSelectedOptions.length == this.protecLen) {
      this.protectCheckbox = true;
    }
  }

  onSelectionProtect(e) {
    console.log(e.option.selected)
    if (e.option.selected == false) {
      this.protectCheckbox = false;
    }
  }

  onNgModelChangeSafeguard(e) {
    this.safeguardSelectedOptions = JSON.parse(JSON.stringify(e));
    console.log(this.hazardSelectedOptions)
    if (this.safeguardSelectedOptions.length == this.safeLen) {
      this.safeCheckbox = true;
    }
  }

  onSelectionSafeguard(e) {
    console.log(e.option.selected)
    if (e.option.selected == false) {
      this.safeCheckbox = false;
    }
  }

  exportToExcel() {
    
    for (var i = 0; i < this.taskSelectedOptions.length; i++) {
      this.taskList.push({ TaskList: this.taskSelectedOptions[i] })
    }
    const taskws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(this.taskList);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, taskws, 'Task');

    for (var i = 0; i < this.subtaskSelectedOptions.length; i++) {
      this.subtaskList.push({ SubtaskList: this.subtaskSelectedOptions[i] })
    }
    const subtaskws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(this.subtaskList);
    xlsx.utils.book_append_sheet(wb, subtaskws, 'Subtask');

    
    for (var i = 0; i < this.hazardSelectedOptions.length; i++) {
      this.hazardList.push({ HazardList: this.hazardSelectedOptions[i] })
    }
    const hazardws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(this.hazardList);
    xlsx.utils.book_append_sheet(wb, hazardws, 'Hazard');

    for (var i = 0; i < this.protectSelectedOptions.length; i++) {
      this.protectList.push({ ProtectList: this.protectSelectedOptions[i] })
    }
    const protectws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(this.protectList);
    xlsx.utils.book_append_sheet(wb, protectws, 'Protective Measure');

    for (var i = 0; i < this.safeguardSelectedOptions.length; i++) {
      this.safegaurdList.push({ SafegaurdList: this.safeguardSelectedOptions[i] })
    }
    const safews: xlsx.WorkSheet = xlsx.utils.json_to_sheet(this.safegaurdList);
    xlsx.utils.book_append_sheet(wb, safews, 'Safeguard');
    xlsx.writeFile(wb, 'ExportParameters.xlsx');

  }

  cancelExcel()
  {
    this.router.navigateByUrl('app/setup')
    // <a class="nav-link" [routerLink]="['/app/setup']" style="color: #615b5b;"> <i class="fa fa-cog" aria-hidden="true">&nbsp;&nbsp;{{ "Setting"}}</i>

  }

}
