import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportParametersComponent } from './export-parameters.component';

describe('ExportParametersComponent', () => {
  let component: ExportParametersComponent;
  let fixture: ComponentFixture<ExportParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportParametersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
