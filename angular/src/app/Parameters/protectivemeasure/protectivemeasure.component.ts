import { Component, OnInit, ViewChild } from '@angular/core';
import { Hazard, TaskServiceServiceProxy, ProtectiveMeasure, ProtectiveMeasureServiceServiceProxy, ProtectiveMeasureDto, CompanyAppServiceProxy, Company } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/shared/common.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { fadeInAnimation } from '@shared/animations';

@Component({
  selector: 'app-protectivemeasure',
  templateUrl: './protectivemeasure.component.html',
  styleUrls: ['./protectivemeasure.component.css'],
  providers: [ProtectiveMeasureServiceServiceProxy, CommonService],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})
export class ProtectivemeasureComponent implements OnInit {

  ProtectiveData: ProtectiveMeasureDto[] = [];
  taskColumns: string[] = ['taskName', 'rsc', 'msc', 'csc', 'actions', 'update'];
  isReadonly: boolean = true;
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  isSelected: boolean = false;
  index: number = undefined;
  CompanyDetails: Company[];
  TaskDetails = new ProtectiveMeasure();
  selectedRowIndex: any;
  @ViewChild('closebutton') closebutton;


  constructor(
    private _commonService: CommonService,
    private companyPortalService: CompanyAppServiceProxy,

    private protectiveservice: ProtectiveMeasureServiceServiceProxy) { }

  ngOnInit(): void {
    this.FetchData();
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })

    this.FetchData();

    this._commonService.TaskRefreshEvent.subscribe(res => {
      if (res == true) {
        this.FetchData();
      }
    })

  }

  FetchData() {
    this.protectiveservice.getAll().subscribe((res) => {
      this.ProtectiveData = res;
      console.log(this.ProtectiveData)
      this.dataSource = new MatTableDataSource(this.ProtectiveData);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

    })
  }

  save(data) {
    console.log(data)
    if (data.csc == null) {
      data.csc = false
    }
    if (data.msc == null) {
      data.msc = false
    }
    if (data.rsc == null) {
      data.rsc = false
    }
    data.companyId = this.CompanyDetails[0].id
    this.protectiveservice.create(data).subscribe(() => {
      abp.notify.success('Added Successfully', 'Protective Measure');
      this.FetchData();
      this.closebutton.nativeElement.click();
      this._commonService.callMethodOfTaskRefreshComponent(true);
    })
  }

  EditTask(task: ProtectiveMeasure) {
    this.protectiveservice.update(task).subscribe(() => {
      abp.notify.success('Updated Successfully', 'Task');
      this._commonService.callMethodOfTaskRefreshComponent(true);

    })
  }


  cellClicked(eleme, i) {
    this.selectedRowIndex = eleme.id;
    //  if (this.index != i || this.index != undefined) {
    //    this.cancelTask()
    //   }
    if (this.isSelected == false) {
      eleme.isReadonly = false
      localStorage.setItem("selectedRow", JSON.stringify(eleme));
      localStorage.setItem("index", i);
      this.isSelected = true;
    }
  }

  updateTask() {
    console.log()
    this.currRow = localStorage.getItem("selectedRow")
    this.currRow.isReadonly = true
  }

  currRow: any;
  cancelTask() {
    this.index = +localStorage.getItem("index");
    this.ProtectiveData[this.index] = JSON.parse(localStorage.getItem("selectedRow"));
    console.log(this.ProtectiveData);
    this.ProtectiveData[this.index].isReadonly = true
    this.dataSource = new MatTableDataSource(this.ProtectiveData);
    this.isSelected = false;
    this.index = undefined;
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  delete(protective: ProtectiveMeasure): void {
    this.protectiveservice.delete(protective.id).subscribe(() => {
      abp.notify.success('Deleted Successfully');
      this.FetchData();
    });
  }

}
