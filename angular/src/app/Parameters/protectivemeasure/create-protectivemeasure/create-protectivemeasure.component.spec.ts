import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProtectivemeasureComponent } from './create-protectivemeasure.component';

describe('CreateProtectivemeasureComponent', () => {
  let component: CreateProtectivemeasureComponent;
  let fixture: ComponentFixture<CreateProtectivemeasureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateProtectivemeasureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProtectivemeasureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
