import { Component, OnInit } from '@angular/core';
import { ProtectiveMeasureServiceServiceProxy, Company, ProtectiveMeasure, CompanyAppServiceProxy } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/shared/common.service';
import { slideInOutAnimation } from '@shared/animations';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-create-protectivemeasure',
  templateUrl: './create-protectivemeasure.component.html',
  styleUrls: ['./create-protectivemeasure.component.css'],
  providers: [ProtectiveMeasureServiceServiceProxy, CommonService],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})
export class CreateProtectivemeasureComponent implements OnInit {

  CompanyDetails: Company[];
  PMDetails = new ProtectiveMeasure();
  MSC: boolean = false
  RSC: boolean = false
  CSC: boolean = false
  Default: boolean = false

  constructor(
    private protectiveService: ProtectiveMeasureServiceServiceProxy,
    private companyPortalService: CompanyAppServiceProxy,
    public _commonService: CommonService,
    private bsModalRef: BsModalRef,

  ) { }

  ngOnInit(): void {
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })
  }

  save(data) {
    data.companyId = 1;
    this.protectiveService.create(data).subscribe((res) => {
      abp.notify.success('Added Successfully', 'Protective Measure');
      this._commonService.callMethodOfTaskRefreshComponent(true);
      this.bsModalRef.hide();

    })
  }
  close() {
    this.bsModalRef.hide();
  }

}
