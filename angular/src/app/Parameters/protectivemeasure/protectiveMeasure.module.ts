import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProtectivemeasureComponent } from './protectivemeasure.component';
import { CreateProtectivemeasureComponent } from './create-protectivemeasure/create-protectivemeasure.component';
import { MaterialModule } from '../../shared/matarial.module';
import { AppModule } from './../../../app/app.module';
import { ProtectiveMeasureServiceServiceProxy, ProtectiveMeasureDto } from '@shared/service-proxies/service-proxies';
import { SharedModule } from '@shared/shared.module';
import { CommonService } from '@app/shared/common.service';
import { Routes, RouterModule } from '@angular/router';

const route: Routes = [
  {
   path: '', component: ProtectivemeasureComponent,
   children: [
    { path: 'addProtectivemeasure', component: CreateProtectivemeasureComponent },
    { path: 'editProtectivemeasure/:id', component: CreateProtectivemeasureComponent },
   ]
  }
]

@NgModule({
  declarations: [
    ProtectivemeasureComponent,
    CreateProtectivemeasureComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    RouterModule.forChild(route),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    NgxPaginationModule,
    MaterialModule,
    AppModule,
    SharedModule
  ],
   providers: [ProtectiveMeasureServiceServiceProxy],
  // entryComponents: [CreateTaskComponent],
})

export class ProtectiveMeasureModule { }
