import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHazardsComponent } from './create-hazards.component';

describe('CreateHazardsComponent', () => {
  let component: CreateHazardsComponent;
  let fixture: ComponentFixture<CreateHazardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHazardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHazardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
