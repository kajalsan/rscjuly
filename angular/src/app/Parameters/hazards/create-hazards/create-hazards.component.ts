import { Component, OnInit } from '@angular/core';
import { Hazard, HazardServiceServiceProxy, CompanyAppServiceProxy, Company } from '@shared/service-proxies/service-proxies';
import { slideInOutAnimation } from '@shared/animations';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-create-hazards',
  templateUrl: './create-hazards.component.html',
  styleUrls: ['./create-hazards.component.css'],
  providers: [HazardServiceServiceProxy],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})

export class CreateHazardsComponent implements OnInit {

  HazardDetails = new Hazard();
  CompanyDetails: Company[];
  MSC: boolean = false
  RSC: boolean = false
  CSC: boolean = false
  Default: boolean = false


  constructor(
    private hazservice: HazardServiceServiceProxy,
    private companyPortalService: CompanyAppServiceProxy,
    private bsModalRef: BsModalRef,

  ) { }

  ngOnInit(): void {
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })
  }

  save(data) {
    data.companyId = 1
    this.hazservice.create(data).subscribe((res) => {
      abp.notify.success('Added Successfully', 'Hazard');
    this.bsModalRef.hide();

    })
  }
  close() {
    this.bsModalRef.hide();
  }

}
