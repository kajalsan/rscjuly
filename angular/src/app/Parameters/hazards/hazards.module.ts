import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { HazardsComponent } from './hazards.component';
import { CreateHazardsComponent } from './create-hazards/create-hazards.component';
import { MaterialModule } from '../../shared/matarial.module';
import { AppModule } from './../../../app/app.module';
import { SharedModule } from '@shared/shared.module';
import { CommonService } from '@app/shared/common.service';
import { HazardServiceServiceProxy } from '@shared/service-proxies/service-proxies';
import { Routes } from '@angular/router';

// import { TaskServiceServiceProxy } from '@shared/service-proxies/service-proxies';
const route: Routes = [
  {
   path: '', component: HazardsComponent,
   children: [
    { path: 'addHazard', component: CreateHazardsComponent },
    { path: 'editHazard/:id', component: CreateHazardsComponent },
   ]
  }
]

@NgModule({
  declarations: [
    HazardsComponent,
    CreateHazardsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forChild(),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    NgxPaginationModule,
    MaterialModule,
    AppModule,
    SharedModule
  ],
  providers: [HazardServiceServiceProxy],
  // entryComponents: [CreateTaskComponent],
})

export class HazardsModule { }
