import { Component, OnInit, ViewChild } from '@angular/core';
import { Hazard, HazardServiceServiceProxy, HazardDto, Company, CompanyAppServiceProxy, Task } from '@shared/service-proxies/service-proxies';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { CommonService } from '@app/shared/common.service';
import { fadeInAnimation } from '@shared/animations';

@Component({
  selector: 'app-hazards',
  templateUrl: './hazards.component.html',
  styleUrls: ['./hazards.component.css'],
  providers: [HazardServiceServiceProxy, CommonService],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})

export class HazardsComponent implements OnInit {

  taskColumns: string[] = ['taskName', 'rsc', 'msc', 'csc', 'actions', 'update'];
  HazardData: HazardDto[] = [];
  isReadonly: boolean = true;
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  isSelected: boolean = false;
  CompanyDetails: Company[];
  index: number = undefined;
  TaskDetails = new Hazard();
  selectedRowIndex: any;
  @ViewChild('closebutton') closebutton;


  constructor(
    private hazservice: HazardServiceServiceProxy,
    private _commonService: CommonService,
    private companyPortalService: CompanyAppServiceProxy,
  ) { }

  ngOnInit(): void {
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })

    this.FetchData();

    this._commonService.TaskRefreshEvent.subscribe(res => {
      if (res == true) {
        this.FetchData();
      }
    })
  }


  FetchData() {
    this.hazservice.getAll().subscribe((res) => {
      this.HazardData = res;
      console.log(this.HazardData);
      this.dataSource = new MatTableDataSource(this.HazardData);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    })
  }

  save(data) {
    console.log(data)
    if (data.csc == null) {
      data.csc = false
    }
    if (data.msc == null) {
      data.msc = false
    }
    if (data.rsc == null) {
      data.rsc = false
    }
    data.companyId = this.CompanyDetails[0].id
    this.hazservice.create(data).subscribe((res) => {
      abp.notify.success('Added Successfully', 'Hazard');
      this.FetchData();
      this._commonService.callMethodOfTaskRefreshComponent(true);
    })
  }

  EditTask(task: Hazard) {
    this.hazservice.update(task).subscribe((res) => {
      abp.notify.success('Updated Successfully', 'Task');
      this._commonService.callMethodOfTaskRefreshComponent(true);

    })
  }
  protected delete(haz: Hazard): void {
    this.hazservice.delete(haz.id).subscribe(() => {
      abp.notify.success('Deleted Successfully');
      this.FetchData();
    });
  }


  cellClicked(eleme, i) {
    this.selectedRowIndex = eleme.id;
    // if (this.index != i || this.index != undefined) {
    //   this.cancelTask()
    //  }
    if (this.isSelected == false) {
      eleme.isReadonly = false
      localStorage.setItem("selectedRow", JSON.stringify(eleme));
      localStorage.setItem("index", i);
      this.isSelected = true;
    }
  }

  currRow: any;
  updateTask() {
    console.log()
    this.currRow = localStorage.getItem("selectedRow")
    this.currRow.isReadonly = true
  }

  cancelTask() {
    this.index = +localStorage.getItem("index");
    this.HazardData[this.index] = JSON.parse(localStorage.getItem("selectedRow"));
    this.HazardData[this.index].isReadonly = true
    this.dataSource = new MatTableDataSource(this.HazardData);
    this.isSelected = false;
    this.index = undefined;
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


}
