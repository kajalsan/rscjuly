import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { TaskComponent } from './task.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { MaterialModule } from '../../shared/matarial.module';
import { AppModule } from './../../../app/app.module';
import { TaskServiceServiceProxy } from '@shared/service-proxies/service-proxies';
import { SharedModule } from '@shared/shared.module';
import { CommonService } from '@app/shared/common.service';
import { RouterModule, Router, Routes } from '@angular/router';

const route: Routes = [
   {
    path: '', component: TaskComponent,
    children: [
     { path: 'addTask', component: CreateTaskComponent },
     { path: 'editTask/:id', component: CreateTaskComponent },
    ]
   }
]

@NgModule({
  declarations: [
    TaskComponent,
    CreateTaskComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    RouterModule.forChild(route),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    NgxPaginationModule,
    MaterialModule,
    AppModule,
    SharedModule
  ],
  providers: [TaskServiceServiceProxy, CommonService],
  entryComponents: [CreateTaskComponent],
})

export class TaskModule { }
