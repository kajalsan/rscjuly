import { Component, OnInit, ViewChild } from '@angular/core';
import { TaskServiceServiceProxy, Task, TaskDto, Company, CompanyAppServiceProxy } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/shared/common.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { fadeInAnimation } from '../../../shared/animations/index';



@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
  providers: [TaskServiceServiceProxy, CommonService],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})

export class TaskComponent implements OnInit {

  TaskdData: TaskDto[] = [];
  taskColumns: string[] = ['taskName', 'rsc', 'msc', 'csc', 'actions', 'update'];
  isReadonly: boolean = true;
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  isSelected: boolean = false;
  TaskDetails = new Task();
  CompanyDetails: Company[];
  MSC: boolean = false
  RSC: boolean = false
  CSC: boolean = false
  Default: boolean = false
  index: number = undefined;
  @ViewChild('closebutton') closebutton;
  @ViewChild('resetButton') resetButton;

  constructor(
    private _commonService: CommonService,
    private companyPortalService: CompanyAppServiceProxy,
    private taskservice: TaskServiceServiceProxy,

  ) {
  }

  ngOnInit(): void {
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })

    this.FetchData();

    this._commonService.TaskRefreshEvent.subscribe(res => {
      if (res == true) {
        this.FetchData();
      }
    })


  }

  FetchData() {
    this.taskservice.getAll().subscribe((res) => {
      this.TaskdData = res;
      console.log(this.TaskdData);
      this.dataSource = new MatTableDataSource(this.TaskdData);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    })
  }

  save(data: Task) {
    console.log(data)
    if (data.csc == null) {
      data.csc = false
    }
    if (data.msc == null) {
      data.msc = false
    }
    if (data.rsc == null) {
      data.rsc = false
    }
    data.companyId = this.CompanyDetails[0].id
    this.taskservice.create(data).subscribe(() => {
      abp.notify.success('Added Successfully', 'Task');
      this.FetchData();
      this.closebutton.nativeElement.click();
      this.resetButton.nativeElement.click();
      this._commonService.callMethodOfTaskRefreshComponent(true);
    })
  }

  clearTask() {
    this.resetButton.nativeElement.click();
  }

  EditTask(task: Task) {
    this.taskservice.update(task).subscribe(() => {
      abp.notify.success('Updated Successfully', 'Task');
      this._commonService.callMethodOfTaskRefreshComponent(true);

    })
  }

  delete(task: Task): void {
    this.taskservice.delete(task.id).subscribe(() => {
      abp.notify.success('Deleted Successfully');
      this.FetchData();
    });
  }

  selectedRowIndex: number

  cellClicked(eleme, i) {
    this.selectedRowIndex = eleme.id
    // if (this.index != i || this.index != undefined) {
    //   this.cancelTask()
    // }
    if (this.isSelected == false) {
      eleme.isReadonly = false
      localStorage.setItem("selectedRow", JSON.stringify(eleme));
      localStorage.setItem("index", i);
      this.isSelected = true;
    }
  }

  updateTask() {
    console.log()
    this.currRow = localStorage.getItem("selectedRow")
    this.currRow.isReadonly = true
  }

  currRow: any;
  cancelTask() {
    this.index = +localStorage.getItem("index");
    this.TaskdData[this.index] = JSON.parse(localStorage.getItem("selectedRow"));
    console.log(this.TaskdData);
    this.TaskdData[this.index].isReadonly = true
    this.dataSource = new MatTableDataSource(this.TaskdData);
    this.isSelected = false;
    this.index = undefined;
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
