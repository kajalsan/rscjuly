import { Component, OnInit } from '@angular/core';
import { TaskServiceServiceProxy, Task, Company, CompanyAppServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '@app/shared/common.service';
import { slideInOutAnimation } from '@shared/animations/index';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css'],
  providers: [TaskServiceServiceProxy, CommonService],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }

})

export class CreateTaskComponent implements OnInit {

  TaskDetails = new Task();
  CompanyDetails: Company[];
  title: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private taskservice: TaskServiceServiceProxy,
    private companyPortalService: CompanyAppServiceProxy,
    public _commonService: CommonService,
    private bsModalRef: BsModalRef,
  ) {

  }

  ngOnInit(): void {
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })
    this.title = 'Add Task';
    const taskID = Number(this.route.snapshot.params['id']);
    if (taskID) {
      this.title = 'Edit Product';
      this.taskservice.getTaskForEdit(taskID).subscribe(res => {
        this.TaskDetails = res;
      }
      );
    }
  }

  save(data) {
    console.log(data)
    data.companyId = 1
    this.taskservice.create(data).subscribe((res) => {
      abp.notify.success('Added Successfully', 'Task');
      this._commonService.callMethodOfTaskRefreshComponent(true);

      this.bsModalRef.hide();
    })
  }
  close(){
    this.bsModalRef.hide();
  }
}
