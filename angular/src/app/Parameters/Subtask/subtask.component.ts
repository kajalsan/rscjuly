import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CreateSubtaskComponent } from './create-subtask/create-subtask.component';
import { SubtaskServiceServiceProxy, Subtask, TaskServiceServiceProxy, TaskDto, SubtaskDto, CompanyAppServiceProxy, Company } from '@shared/service-proxies/service-proxies';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { CommonService } from '@app/shared/common.service';
import { fadeInAnimation } from '@shared/animations';

export class Group {
  level = 0;
  parent: Group;
  expanded = true;
  totalCounts = 0;
  get visible(): boolean {
    return !this.parent || (this.parent.visible && this.parent.expanded);
  }
}

@Component({
  selector: 'app-subtask',
  templateUrl: './subtask.component.html',
  styleUrls: ['./subtask.component.css'],
  providers: [SubtaskServiceServiceProxy, TaskServiceServiceProxy, CommonService],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})

export class SubtaskComponent implements OnInit {
  subtaskData: SubtaskDto[] = [];
  TaskdData: TaskDto[] = [];
  subTaskColumns: string[] = ['subtaskName', 'rsc', 'msc', 'csc', 'defaultcheck', 'actions'];
  CompanyDetails: Company[];
  isReadonly: boolean = true;
  // dataSource: MatTableDataSource<any>;
  public dataSource = new MatTableDataSource<any | Group>([]);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  isSelected: boolean = false;
  index: number;
  _alldata: any[];
  columns: any[];
  displayedColumns: string[];
  groupByColumns: string[] = [];
  data: any;
  TaskDetails = new Subtask();
  @ViewChild('closebutton') closebutton;

  constructor(
    private _modalService: BsModalService,
    private subtaskservice: SubtaskServiceServiceProxy,
    private taskservice: TaskServiceServiceProxy,
    private companyPortalService: CompanyAppServiceProxy,
    private _commonService: CommonService,
  ) {
    this.columns = [{

      field: 'subtaskName'
    }, {
      field: 'rsc'
    }, {
      field: 'msc'
    }, {
      field: 'csc'
    }, {
      field: 'actions'
    }, {
      field: 'update'
    }];
    this.displayedColumns = this.columns.map(column => column.field);
    console.log(this.displayedColumns)
    this.groupByColumns = ['taskId'];
  }

  ngOnInit(): void {
    this.fetchData();
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })
    this.taskservice.getAll().subscribe((res) => {
      this.TaskdData = res;
    })
  }



  protected delete(task: Subtask): void {
    this.subtaskservice.delete(task.id).subscribe(() => {
      abp.notify.success('Deleted Successfully');
      this.fetchData();
    });
  }

  save(data: Subtask) {
    console.log(data)
    if (data.csc == null) {
      data.csc = false
    }
    if (data.msc == null) {
      data.msc = false
    }
    if (data.rsc == null) {
      data.rsc = false
    }
    data.companyId = this.CompanyDetails[0].id
    this.subtaskservice.create(data).subscribe((res) => {
      abp.notify.success('Added Successfully', 'Subtask');
      this.closebutton.nativeElement.click();
      // $("#myModal2").modal("hide");
      this.fetchData();
      // this._commonService.callMethodOfTaskRefreshComponent(true);
    })
  }

  EditTask(task) {
    if (task.msc == "YES") {
      task.msc = true;
    }
    else {
      task.msc = false
    }
    if (task.csc == "YES") {
      task.csc = true;
    }
    else {
      task.csc = false
    }
    if (task.rsc == "YES") {
      task.rsc = true;
    }
    else {
      task.rsc = false
    }
    if (task.defaultcheck == "YES") {
      task.defaultcheck = true;
    }
    else {
      task.defaultcheck = false
    }
    this.subtaskservice.update(task).subscribe((res) => {
      abp.notify.success('Updated Successfully', 'Subtask');
      this.fetchData()
    })
  }

  fetchData() {
    this.subtaskservice.getAll()
      .subscribe(
        (res: any) => {
          console.log(res)
          this.data = JSON.parse(JSON.stringify(res));
          this.data.forEach((item, index) => {
            item.id = index + 1;
          });
          this._alldata = this.data;
          console.log(this._alldata, 'data')
          this.dataSource.data = this.addGroups(this._alldata, this.groupByColumns);
          this.dataSource.filterPredicate = this.customFilterPredicate.bind(this);
          this.dataSource.filter = performance.now().toString();
        },
        (err: any) => console.log(err)
      );
  }

  groupBy(event, column) {
    event.stopPropagation();
    this.checkGroupByColumn(column.field, true);
    this.dataSource.data = this.addGroups(this._alldata, this.groupByColumns);
    this.dataSource.filter = performance.now().toString();
  }

  checkGroupByColumn(field, add) {
    let found = null;
    for (const column of this.groupByColumns) {
      if (column === field) {
        found = this.groupByColumns.indexOf(column, 0);
      }
    }
    if (found != null && found >= 0) {
      if (!add) {
        this.groupByColumns.splice(found, 1);
      }
    } else {
      if (add) {
        this.groupByColumns.push(field);
      }
    }
  }

  unGroupBy(event, column) {
    event.stopPropagation();
    this.checkGroupByColumn(column.field, false);
    this.dataSource.data = this.addGroups(this._alldata, this.groupByColumns);
    this.dataSource.filter = performance.now().toString();
  }

  // below is for grid row grouping
  customFilterPredicate(data: any | Group, filter: string): boolean {
    return (data instanceof Group) ? data.visible : this.getDataRowVisible(data);
  }

  getDataRowVisible(data: any): boolean {
    const groupRows = this.dataSource.data.filter(
      row => {
        if (!(row instanceof Group)) {
          return false;
        }
        let match = true;
        this.groupByColumns.forEach(column => {
          if (!row[column] || !data[column] || row[column] !== data[column]) {
            match = false;
          }
        });
        return match;
      }
    );

    if (groupRows.length === 0) {
      return true;
    }
    const parent = groupRows[0] as Group;
    return parent.visible && parent.expanded;
  }

  groupHeaderClick(row) {
    row.expanded = !row.expanded;
    this.dataSource.filter = performance.now().toString();  // bug here need to fix
  }

  addGroups(data: any[], groupByColumns: string[]): any[] {
    const rootGroup = new Group();
    rootGroup.expanded = true;
    return this.getSublevel(data, 0, groupByColumns, rootGroup);
  }

  getSublevel(data: any[], level: number, groupByColumns: string[], parent: Group): any[] {
    if (level >= groupByColumns.length) {
      return data;
    }
    const groups = this.uniqueBy(
      data.map(
        row => {
          const result = new Group();
          result.level = level + 1;
          result.parent = parent;
          for (let i = 0; i <= level; i++) {
            result[groupByColumns[i]] = row[groupByColumns[i]];
          }
          return result;
        }
      ),
      JSON.stringify);

    const currentColumn = groupByColumns[level];
    let subGroups = [];
    groups.forEach(group => {
      const rowsInGroup = data.filter(row => group[currentColumn] === row[currentColumn]);
      group.totalCounts = rowsInGroup.length;
      const subGroup = this.getSublevel(rowsInGroup, level + 1, groupByColumns, group);
      subGroup.unshift(group);
      subGroups = subGroups.concat(subGroup);
    });
    return subGroups;
  }

  uniqueBy(a, key) {
    const seen = {};
    return a.filter((item) => {
      const k = key(item);
      return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    });
  }

  isGroup(index, item): boolean {
    return item.level;
  }

  cancelTask() {
    this.index = +localStorage.getItem("index");
    this.isSelected = false;
    this._alldata[this.index] = JSON.parse(localStorage.getItem("selectedRow"));
    this._alldata[this.index].isReadonly = true
    console.log(this._alldata, 'data')
    this.dataSource.data = this.addGroups(this._alldata, this.groupByColumns);
    this.index = undefined;
    // this.fetchData()
    // this.dataSource = new MatTableDataSource(this.TaskdData);
  }

  selectedRowIndex: number

  cellClicked(eleme, i) {
    this.selectedRowIndex = eleme.id
    // if (this.index != i || this.index != undefined) {
    //   this.cancelTask()
    // }
    if (this.isSelected == false) {
      eleme.isReadonly = false
      localStorage.setItem("selectedRow", JSON.stringify(eleme));
      // i = i - 1;
      localStorage.setItem("index", i);
      this.isSelected = true;
    }
  }



  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
