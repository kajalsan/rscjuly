import { BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit } from '@angular/core';
import { Subtask, SubtaskServiceServiceProxy, Company, CompanyAppServiceProxy } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/shared/common.service';
import { slideInOutAnimation } from '@shared/animations';

@Component({
  selector: 'app-create-subtask',
  templateUrl: './create-subtask.component.html',
  styleUrls: ['./create-subtask.component.css'],
  providers: [SubtaskServiceServiceProxy, CommonService],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }

})
export class CreateSubtaskComponent implements OnInit {

  subtaskdetails = new Subtask();
  CompanyDetails: Company[];
  MSC: boolean = false
  RSC: boolean = false
  CSC: boolean = false
  Default: boolean = false


  constructor(
    private subtaskservice: SubtaskServiceServiceProxy,
    private companyPortalService: CompanyAppServiceProxy,
    public _commonService : CommonService,
    private bsModalRef: BsModalRef,

  ) { }

  ngOnInit(): void {
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })
  }


  save(data) {
    console.log(data)
    data.companyId =1
    this.subtaskservice.create(data).subscribe(res => {
      abp.notify.success('Added Successfully', 'Subtask');
      this._commonService.callMethodOfTaskRefreshComponent(true);
    this.bsModalRef.hide();

    })
  }

  close(){
    this.bsModalRef.hide();
  }
}
