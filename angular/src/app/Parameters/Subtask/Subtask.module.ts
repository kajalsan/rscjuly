import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { SubtaskComponent } from './subtask.component';
import { CreateSubtaskComponent } from './create-subtask/create-subtask.component';
import { MaterialModule } from '../../shared/matarial.module';
import { AppModule } from './../../../app/app.module';
import { SharedModule} from 'shared/shared.module';
import { TaskServiceServiceProxy, SubtaskServiceServiceProxy } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/shared/common.service';

@NgModule({
  declarations: [
    SubtaskComponent,
    CreateSubtaskComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forChild(),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    NgxPaginationModule,
    MaterialModule,
    AppModule,
    SharedModule
  ],
  providers: [SubtaskServiceServiceProxy, CommonService],
  // entryComponents: [CreateTaskComponent],
})

export class SubTaskModule { }
