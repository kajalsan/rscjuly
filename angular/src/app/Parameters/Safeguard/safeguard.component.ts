import { BsModalService } from 'ngx-bootstrap/modal';
import { SafeguardServiceServiceProxy, Safeguard, SafeguardDto, Company, CompanyAppServiceProxy } from '@shared/service-proxies/service-proxies';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from '@app/shared/common.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { fadeInAnimation } from '../../../shared/animations/index';

@Component({
  selector: 'app-safeguard',
  templateUrl: './safeguard.component.html',
  styleUrls: ['./safeguard.component.css'],
  providers: [SafeguardServiceServiceProxy, CommonService],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})

export class SafeguardComponent implements OnInit {
  safeData: SafeguardDto[] = [];
  taskColumns: string[] = ['taskName', 'rsc', 'msc', 'csc', 'actions', 'update'];
  isReadonly: boolean = true;
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  isSelected: boolean = false;
  TaskDetails = new Safeguard();
  CompanyDetails: Company[];
  MSC: boolean = false
  RSC: boolean = false
  CSC: boolean = false
  Default: boolean = false
  index: number = undefined;
  @ViewChild('closebutton') closebutton;


  constructor(
    private safeguardkservice: SafeguardServiceServiceProxy,
    private _commonService: CommonService,
    private companyPortalService: CompanyAppServiceProxy,
  ) { }

  ngOnInit(): void {
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })
    this.fetchData();
    this._commonService.TaskRefreshEvent.subscribe(res => {
      if (res == true) {
        this.fetchData();
      }
    })

  }

  protected delete(task: Safeguard): void {
    this.safeguardkservice.delete(task.id).subscribe(() => {
      abp.notify.success('Deleted Successfully');
      this.fetchData();
    });
  }

  fetchData() {
    this.safeguardkservice.getAll().subscribe((res) => {
      this.safeData = res;
      console.log(this.safeData)
      this.dataSource = new MatTableDataSource(this.safeData);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

    })
  }

  save(data) {
    console.log(data)
    if (data.csc == null) {
      data.csc = false
    }
    if (data.msc == null) {
      data.msc = false
    }
    if (data.rsc == null) {
      data.rsc = false
    }
    data.companyId = this.CompanyDetails[0].id
    this.safeguardkservice.create(data).subscribe(() => {
      abp.notify.success('Added Successfully', 'Safeguard');
      this.fetchData();
      this.closebutton.nativeElement.click();
      this._commonService.callMethodOfTaskRefreshComponent(true);
    })
  }

  EditTask(task: Safeguard) {
    this.safeguardkservice.update(task).subscribe(() => {
      abp.notify.success('Updated Successfully', 'Task');
      this._commonService.callMethodOfTaskRefreshComponent(true);
      this.fetchData();

    })
  }

  selectedRowIndex: number
  cellClicked(eleme, i) {
    this.selectedRowIndex = eleme.id;
    //  if (this.index != i || this.index != undefined) {
    //    this.cancelTask()
    //   }
    if (this.isSelected == false) {
      eleme.isReadonly = false
      localStorage.setItem("selectedRow", JSON.stringify(eleme));
      localStorage.setItem("index", i);
      this.isSelected = true;
    }
  }

  updateTask() {
    console.log()
    this.currRow = localStorage.getItem("selectedRow")
    this.currRow.isReadonly = true
  }

  currRow: any;
  cancelTask() {
    this.index = +localStorage.getItem("index");
    this.safeguardkservice[this.index] = JSON.parse(localStorage.getItem("selectedRow"));
    console.log(this.safeguardkservice);
    this.safeguardkservice[this.index].isReadonly = true
    this.dataSource = new MatTableDataSource(this.safeData);
    this.isSelected = false;
    this.index = undefined;
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


}
