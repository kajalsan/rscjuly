import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSafeguardComponent } from './create-safeguard.component';

describe('CreateSafeguardComponent', () => {
  let component: CreateSafeguardComponent;
  let fixture: ComponentFixture<CreateSafeguardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSafeguardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSafeguardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
