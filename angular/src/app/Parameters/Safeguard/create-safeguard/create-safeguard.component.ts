import { Component, OnInit } from '@angular/core';
import { Subtask, SubtaskServiceServiceProxy, Company, CompanyAppServiceProxy, SafeguardServiceServiceProxy, Safeguard } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/shared/common.service';
import { slideInOutAnimation } from '@shared/animations';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-create-safeguard',
  templateUrl: './create-safeguard.component.html',
  styleUrls: ['./create-safeguard.component.css'],
  providers: [SafeguardServiceServiceProxy, CommonService],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }


})
export class CreateSafeguardComponent implements OnInit {

  safegaurddetails = new Safeguard();
  CompanyDetails: Company[];
  MSC: boolean = false
  RSC: boolean = false
  CSC: boolean = false
  Default: boolean = false


  constructor(
    private safeguardkservice: SafeguardServiceServiceProxy,
    private companyPortalService: CompanyAppServiceProxy,
    public _commonService: CommonService,
    private bsModalRef: BsModalRef,

  ) { }

  ngOnInit(): void {
    this.companyPortalService.getByCustomerId(1).subscribe(rescomp => {
      this.CompanyDetails = rescomp
    })
  }


  save(data) {
    console.log(data)
    data.companyId = 1
    this.safeguardkservice.create(data).subscribe(res => {
      abp.notify.success('Added Successfully', 'Safeguard');
      this._commonService.callMethodOfTaskRefreshComponent(true);
      this.bsModalRef.hide();

    })
  }
  close() {
    this.bsModalRef.hide();
  }
}
