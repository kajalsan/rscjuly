import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { SafeguardComponent } from './safeguard.component';
import { CreateSafeguardComponent } from './create-safeguard/create-safeguard.component';
import { MaterialModule } from '../../shared/matarial.module';
import { AppModule } from '../../app.module';
import { CommonService } from './../../shared/common.service';
import { SharedModule} from 'shared/shared.module';
import { SafeguardServiceServiceProxy } from '@shared/service-proxies/service-proxies';
import { Routes, RouterModule } from '@angular/router';

const route: Routes = [
  {
   path: '', component: SafeguardComponent,
   children: [
    { path: 'addSafeguard', component: CreateSafeguardComponent },
    { path: 'editSafeguard/:id', component: CreateSafeguardComponent },
   ]
  }
]

@NgModule({
  declarations: [
    SafeguardComponent,
    CreateSafeguardComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    RouterModule.forChild(route),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    NgxPaginationModule,
    MaterialModule,
    AppModule,
    SharedModule
  ],
   providers: [SafeguardServiceServiceProxy, CommonService],
  // entryComponents: [CreateTaskComponent],
})

export class SafeguardModule { }
