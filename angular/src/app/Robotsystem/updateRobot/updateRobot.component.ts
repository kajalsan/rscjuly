import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { RobotTaskAssismentServiceProxy, RobotTaskAppServiceProxy } from '@shared/service-proxies/service-proxies';

// import { RobotTaskAppServiceProxy, RobotSystem } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';


@Component({
  selector: 'app-updateRobot',
  templateUrl: './updateRobot.component.html',
})
export class updateRobotComponent implements OnInit {



  constructor(
    private router: Router,
    private _robotsystemAppServiceProxy: RobotTaskAppServiceProxy,



  ) { }
  RobotTaskId:any;
  ngOnInit(): void {
    this.getAllRobot();


  }

  trackingTableColumns: string[] = ['dateOrigin', 'robotName', 'asset', 'integratorManufacturer', 'projectManager', 'buildingLocation', 'Edit', 'Delete', 'info'];
  // trackingTableColumns: string[] = ['taskName', 'subTaskName', 'hazardName'];
  //  'S', 'E', 'A', 'R',  'Pl-Cat','S', 'E', 'A',

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  RobotTaskData: any;

  getAllRobot() {
    this._robotsystemAppServiceProxy.getBycompanyid().subscribe(result => {
      this.RobotTaskData = result;
      this.dataSource = new MatTableDataSource(this.RobotTaskData);
      this.dataSource.paginator = this.paginator;
    })
  }

  AddNewRobot() {
    this.router.navigate(['app/add-robot']);

  }

  editRobot(IdRobot) {
    var tempId = IdRobot.toString();
    localStorage.setItem('RobotTaskId', tempId);
    this.router.navigate(['app/RiskMatrix']);

  }

  DeleteRobot(Id) {
    this._robotsystemAppServiceProxy.deleteRobot(Id).subscribe(result => {
      this.getAllRobot();
    })

  }

  EditRobotSystem(Id){
    var tempId=Id.toString();
    var flag = "true";
    localStorage.setItem('TaskAssismentFlag', flag)

    localStorage.setItem('RobotId',tempId);
    this.router.navigate(['app/add-robot']);

  }
}
