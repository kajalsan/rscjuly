import { Component, OnInit, NgModule, OnDestroy } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { RobotTaskAppServiceProxy, RobotSystem } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';


@Component({
  selector: 'app-robot',
  templateUrl: './robotsystemadd.component.html',
  styleUrls: ['./robotsystemadd.component.css'],
  animations: [appModuleAnimation()],
})
export class robotsystemaddComponent implements OnInit, OnDestroy {
  firstNameAutofilled: boolean;
  lastNameAutofilled: boolean;
  robotSystemdata: RobotSystem = new RobotSystem();


  constructor(
    private router: Router,
    private _robotsystemAppServiceProxy: RobotTaskAppServiceProxy,
  ) { }
  RobotId: any;
  tempflag: any;
  ngOnInit(): void {
    this.RobotId = localStorage.getItem('RobotId');
    this.tempflag = localStorage.getItem('TaskAssismentFlag')
    if (this.tempflag == 'true') {
      this.GetById(this.RobotId)

    }

  }
  ngOnDestroy() {
    localStorage.removeItem("TaskAssismentFlag")
  }
  saveAddrobot(data) {
    console.log(data);
    if (this.tempflag == 'true') {
      data.id = this.RobotId;
      this._robotsystemAppServiceProxy.updateRobot(data).subscribe(result => {
        localStorage.removeItem('TaskAssismentFlag')
        localStorage.removeItem('RobotId')
        this.router.navigate(['app/update-robot']);

      })
    }
    else {
      this._robotsystemAppServiceProxy.create(data).subscribe(result => {
        var tempId = result.toString();
        localStorage.setItem('RobotTaskId', tempId);
        this.router.navigate(['app/RiskMatrix']);

      })
    }
  }

  cancel() {
    this.router.navigate(['app/dashboard']);

  }


  GetById(RobotId) {
    this._robotsystemAppServiceProxy.getById(RobotId).subscribe(result => {
      console.log(result);

      this.robotSystemdata = result[0];
    })
  }
}
