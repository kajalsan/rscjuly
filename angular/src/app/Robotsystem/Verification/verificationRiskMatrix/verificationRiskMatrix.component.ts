import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
// import { RobotTaskAppServiceProxy, RobotSystem } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { RobotTaskAssismentServiceProxy } from '@shared/service-proxies/service-proxies';


@Component({
  selector: 'app-VerificationRisk',
  templateUrl: './verificationRiskMatrix.component.html',
})
export class VerificationriskMatrixComponent implements OnInit {



  constructor(
    private router: Router,
    private _robotTaskAppServiceProxy: RobotTaskAssismentServiceProxy,

    // private _RobotTaskAppServiceProxy: RobotTaskAssismentServiceProxy,

  ) { }
  Comments: any;
  RobotTaskId: any;
  ngOnInit(): void {
    this.RobotTaskId = localStorage.getItem('RobTaskIdsingle');
  }



  RiskMatrixArray: any[] = [
    { ID: 'CheckBox0', ischecked: false },
    { ID: 'CheckBox1', ischecked: false },
    { ID: 'CheckBox2', ischecked: false },
    { ID: 'CheckBox3', ischecked: false },
    { ID: 'CheckBox4', ischecked: false },
    { ID: 'CheckBox5', ischecked: false },
    { ID: 'CheckBox6', ischecked: false },
    { ID: 'CheckBox7', ischecked: false },
    { ID: 'CheckBox8', ischecked: false },
    { ID: 'CheckBox9', ischecked: false },
    { ID: 'CheckBox10', ischecked: false },
    { ID: 'CheckBox11', ischecked: false },
  ]


  RiskMatrixBindarray: any;
  EarlyCheckForm: any;


  onEarlyChange(event) {
    this.EarlyCheckForm = event.checked
    console.log(event.source.name);

    const checkedElementName = event.source.name
    this.RiskMatrixBindarray = event.source.name
    const isAlreadyChecked = this.RiskMatrixArray.map(i => i.ID).indexOf(checkedElementName) === -1

    if (isAlreadyChecked) {
    }
    else {
      this.RiskMatrixArray = this.RiskMatrixArray.map(i => {
        return {
          ID: i.ID,
          ischecked: i.ID !== checkedElementName ? false : true
        }
      });
    }
  }



  back() {
    this.router.navigate(['app/Verification-punch']);
  }
verificationdata:any;

  VerificationRisk(data) {
    if (this.RiskMatrixBindarray == "CheckBox0") {
      data.verification_S = 'S1';
      data.verification_E = 'EO';
      data.verification_A = 'AO';
      data.verification = 'N';
      data.documentName = '1'
    }
    else if (this.RiskMatrixBindarray == "CheckBox1") {
      data.verification_S = 'S1';
      data.verification_E = 'E1';
      data.verification_A = 'A1';
      data.verification = 'N';
      data.documentName = '1'
    }
    else if (this.RiskMatrixBindarray == "CheckBox2") {
      data.verification_S = 'S1';
      data.verification_E = 'E2';
      data.verification_A = 'A2';
      data.verification = 'L';
      data.documentName = '2';

    }
    else if (this.RiskMatrixBindarray == "CheckBox3") {
      data.verification_S = 'S1';
      data.verification_E = 'E2';
      data.verification_A = ' ';
      data.verification = 'L';
      data.riskMatrix_PLLcAT = 'PL c Cat. 2';
    }
    else if (this.RiskMatrixBindarray == "CheckBox4") {
      data.verification_S = 'S2';
      data.verification_E = 'EO';
      data.verification_A = ' ';
      data.verification = 'L';
      data.documentName = '4';

    }
    else if (this.RiskMatrixBindarray == "CheckBox5") {
      data.verification_S = 'S2';
      data.verification_E = 'E1';
      data.verification_A = '';
      data.verification = 'M';
      data.documentName = '5';

    }
    else if (this.RiskMatrixBindarray == "CheckBox6") {
      data.verification_S = 'S2';
      data.verification_E = 'E2';
      data.verification_A = 'A1';
      data.verification = 'M';
      data.documentName = '6';

    }
    else if (this.RiskMatrixBindarray == "CheckBox7") {
      data.verification_S = 'S2';
      data.verification_E = 'E2';
      data.verification_A = 'A2';
      data.verification = 'H';
      data.documentName = '7';

    }
    else if (this.RiskMatrixBindarray == "CheckBox8") {
      data.verification_S = 'S3';
      data.verification_E = 'EO';
      data.verification_A = '';
      data.verification = 'L';
      data.documentName = '8';

    } else if (this.RiskMatrixBindarray == "CheckBox9") {
      data.verification_S = 'S3';
      data.verification_E = 'E1';
      data.verification_A = '';
      data.verification = 'L';
      data.documentName = '9';

    }
    else if (this.RiskMatrixBindarray == "CheckBox10") {
      data.verification_S = 'S3';
      data.verification_E = 'E2';
      data.verification_A = 'A1/A2';
      data.verification = 'H';
      data.documentName = '10';

    }
    else {
      data.verification_S = 'S3';
      data.verification_E = 'E2';
      data.verification_A = 'A3';
      data.verification = 'VH';
      data.documentName = '11';

    }
    data.id = this.RobotTaskId;
    console.log(data);
    this._robotTaskAppServiceProxy.verificationUpdate(data).subscribe(result => {
      // abp.notify.success('Updated Successfully', 'Robot Assisment Task');
      this.router.navigate(['app/RiskMatrix']);

    })
  }
}



