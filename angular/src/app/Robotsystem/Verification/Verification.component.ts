import { Component, OnInit, NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { RobotTaskAssismentServiceProxy } from '@shared/service-proxies/service-proxies';
// import { RobotTaskAppServiceProxy, RobotSystem } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';



@Component({
  selector: 'app-VerificationTask',
  templateUrl: './Verification.component.html',
})
export class VerificationComponent implements OnInit {



  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<VerificationComponent>,
    private _RobotTaskAppServiceProxy: RobotTaskAssismentServiceProxy,


  ) { }
  RobotTaskId: any;
  ngOnInit(): void {
    this.RobotTaskId = localStorage.getItem('RobotTaskId');
    this.GetOldTask(this.RobotTaskId);

  }
  oldtaskD: any;
  newtaskD: any;

  close() {
    this.dialogRef.close();


  }
  taskId: any;
  onTaskSelection(value) {
    console.log(value)
    this.taskId = value;
  }
  taskData: any;

  taskoldData: any;
  GetOldTask(RobotTaskId) {
    this._RobotTaskAppServiceProxy.getRobotTaskAssisment(RobotTaskId).subscribe(result => {
      this.taskoldData = result;

    })
  }
  Verify() {
    this._RobotTaskAppServiceProxy.getRobotByIDTaskAssisment(this.taskId).subscribe(result => {
      var verificationdone = result[0].verification;
      if (verificationdone == null) {
        localStorage.setItem('RobTaskIdsingle', this.taskId);
        this.dialogRef.close();
        this.router.navigate(['app/Verificationtask']);
      }
      else {
        alert('Verification is already Done')
        this.dialogRef.close();
      }
    })
  }
}
