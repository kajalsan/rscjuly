import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
// import { RobotTaskAppServiceProxy, RobotSystem } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { RobotTaskAssismentServiceProxy } from '@shared/service-proxies/service-proxies';


@Component({
  selector: 'app-VerificationTasK',
  templateUrl: './VerificationTask.component.html',
})
export class VerificationTaskComponent implements OnInit {



  constructor(
    private router: Router,
    private _robotTaskAssismentServiceProxy: RobotTaskAssismentServiceProxy,

    // private _RobotTaskAppServiceProxy: RobotTaskAssismentServiceProxy,

  ) { }
  Comments: any;
  RobotTaskId:any;
  ngOnInit(): void {
    // this.GetOldTask();
    var tempId=localStorage.getItem('RobTaskIdsingle')

    this.getTaskAssisment(tempId);
  }
  trackingTableColumns: string[] = ['Edit', 'taskName', 'subTaskName', 'hazardName', 'riskMatrix_s', 'riskMatrix_E', 'riskMatrix_A', 'riskMatrix_R', 'riskMatrix_PLLcAT', 'safeguardsName', 'protectiveName', 'Ss', 'Ee', 'Aa', 'comment', 'verification', 'validation','documentPath'];
  // trackingTableColumns: string[] = ['taskName', 'subTaskName', 'hazardName'];
  //  'S', 'E', 'A', 'R',  'Pl-Cat','S', 'E', 'A',

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  RobotAssismentData: any;
  RobotAssismenTask: any;

  getTaskAssisment(tempId) {
    this._robotTaskAssismentServiceProxy.getRobotByIDTaskAssisment(tempId).subscribe(result => {

      // this.RobotAssismentData = result;
      // console.log(result);
      this.RobotAssismenTask=result[0].taskName;
      this.RobotAssismentData = result;
      this.dataSource = new MatTableDataSource(this.RobotAssismentData);
      this.dataSource.paginator = this.paginator;

    })
  }
  VerificationType: any;
  foods4 = [
    { viewValue: 'Verification requires re-assessing the values of SEVERITY, EXPOSURE and AVOIDANCE. Check the box and press Continue Verification button.' },
    { viewValue: 'Denied' },
  ]

  onChange(value) {

  }


  earlyArray: any[] = [{ ID: 'Approve', ischecked: false },
  { ID: 'Decline', ischecked: false },]


  earlyarray: any;
  EarlyCheckForm: any;


  onEarlyChange(event) {
    this.EarlyCheckForm = event.checked
    // //console.log(event.source.name);

    const checkedElementName = event.source.name
    this.earlyarray = event.source.name
     
    const isAlreadyChecked = this.earlyArray.map(i => i.ID).indexOf(checkedElementName) === -1

    if (isAlreadyChecked) {
    }
    else {
      this.earlyArray = this.earlyArray.map(i => {
        return {
          ID: i.ID,
          ischecked: i.ID !== checkedElementName ? false : true
        }
      });
    }
  }
  Verification() {
   
    this.router.navigate(['app/Verification-punch']);

   
  }
  EditTask(id) {
    localStorage.setItem('TaskAssismentId', id)
    var flag = "true";
    localStorage.setItem('TaskAssismentFlag', flag)

    this.router.navigate(['app/Add-Task']);
  }

  back(){
    this.router.navigate(['app/RiskMatrix']);
  }
}
