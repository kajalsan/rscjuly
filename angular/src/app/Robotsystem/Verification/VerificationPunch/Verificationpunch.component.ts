import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
// import { RobotTaskAppServiceProxy, RobotSystem } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { RobotTaskAssismentServiceProxy, SafeguardServiceServiceProxy, ProtectiveMeasureServiceServiceProxy } from '@shared/service-proxies/service-proxies';


@Component({
  selector: 'app-Verificationpunch',
  templateUrl: './Verificationpunch.component.html',
})
export class VerificationpunchComponent implements OnInit {



  constructor(
    private router: Router,
    private _robotTaskAssismentServiceProxy: RobotTaskAssismentServiceProxy,
    private _safeguardServiceServiceProxy: SafeguardServiceServiceProxy,
    private _protectiveMeasureServiceServiceProxy: ProtectiveMeasureServiceServiceProxy,
    // private _RobotTaskAppServiceProxy: RobotTaskAssismentServiceProxy,

  ) { }
  Comments: any;
  RobotTaskId: any;
  ngOnInit(): void {
    // this.GetOldTask();
    this.RobotTaskId = localStorage.getItem('RobTaskIdsingle');

    this.getTaskAssisment(this.RobotTaskId);
    this.Getssafegaurd()
    this.GetProtective()
  }
  trackingTableColumns: string[] = ['Actions', 'taskName', 'subTaskName', 'hazardName', 'safeguardsName', 'protectiveName', 'comment', 'verification', 'validation'];
  // trackingTableColumns: string[] = ['taskName', 'subTaskName', 'hazardName'];
  //  'S', 'E', 'A', 'R',  'Pl-Cat','S', 'E', 'A',

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  RobotAssismentData: any;
  RobotAssismenTask: any;
  getTaskAssisment(RobotTaskId) {
    this._robotTaskAssismentServiceProxy.getRobotByIDTaskAssisment(RobotTaskId).subscribe(result => {
      this.RobotAssismenTask = result[0].taskName;
      this.safegaurdD = result[0].safeguardsName.split(',');
      this.ProtectiveD = result[0].protectiveName.split(',')
    })
  }
  VerificationType: any;
  foods4 = [
    { viewValue: 'Verification requires re-assessing the values of SEVERITY, EXPOSURE and AVOIDANCE. Check the box and press Continue Verification button.' },
    { viewValue: 'Denied' },
  ]

  onChange(value) {

  }

  applyFilterprotective(filterValue1: string) {
    console.log(filterValue1);
    this.Protective = this.Protective1.filter((i) => i.protectiveName.toLowerCase().includes(filterValue1.toLowerCase()));

  }
  applyFiltersafe(filterValue1: string) {
    console.log(filterValue1);
    this.safegaurdData = this.safegaurdData1.filter((i) => i.safeguardName.toLowerCase().includes(filterValue1.toLowerCase()));

  }
  safegaurdId: any
  protectiveId: any;
  onsafegaurdSelection(value) {
    var temp = Array(value).toString()
    console.log(temp)
    this.safegaurdId = temp;
  }

  onProtectiveSelection(value) {
    var temp = Array(value).toString()
    console.log(temp)
    this.protectiveId = temp;
  }

  earlyArray: any[] = [{ ID: 'Approve', ischecked: false },
  { ID: 'Decline', ischecked: false },]


  earlyarray: any;
  EarlyCheckForm: any;


  onEarlyChange(event) {
    this.EarlyCheckForm = event.checked
    // //console.log(event.source.name);

    const checkedElementName = event.source.name
    this.earlyarray = event.source.name
    const isAlreadyChecked = this.earlyArray.map(i => i.ID).indexOf(checkedElementName) === -1

    if (isAlreadyChecked) {
    }
    else {
      this.earlyArray = this.earlyArray.map(i => {
        return {
          ID: i.ID,
          ischecked: i.ID !== checkedElementName ? false : true
        }
      });
    }
  }
  Verification() {
    this.router.navigate(['app/verification-riskmatrix']);
  }

  EditTask(id) {
    localStorage.setItem('TaskAssismentId', id)
    var flag = "true";
    localStorage.setItem('TaskAssismentFlag', flag)

    this.router.navigate(['app/Add-Task']);
  }
  back() {
    this.router.navigate(['app/Verificationtask']);
  }


  safegaurdData: any;
  safegaurdData1: any;
  safegaurdD: any;

  Getssafegaurd() {
    this._safeguardServiceServiceProxy.getAll().subscribe(result => {
      this.safegaurdData = result;
      this.safegaurdData1 = result;
    })
  }


  Protective: any;
  Protective1: any;
  ProtectiveD: any;

  GetProtective() {
    this._protectiveMeasureServiceServiceProxy.getAll().subscribe(result => {
      this.Protective = result;
      this.Protective1 = result;
    })
  }


}
