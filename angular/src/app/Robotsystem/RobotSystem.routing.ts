import { Routes } from '@angular/router';
import { robotsystemaddComponent } from './robotsystemadd.component'
import { RobotMatrixAssesment } from './robot-assesmentmatrix/robot-assesmentmatrix.component';
import { RobotTaskcomponent } from './RObotTask/robot-Task.component';

export const RobotsystemRouter: Routes = [
    { path: '', component: robotsystemaddComponent },
    // { path: 'RiskAssMatrix', component: RobotMatrixAssesment },
     { path: 'app/RiskMatrix', component: RobotMatrixAssesment },
    { path: 'Add-Task', component: RobotTaskcomponent },

]
