import { Component, OnInit, ViewChild, Injector, OnDestroy, Input } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { RobotTaskAssismentServiceProxy, RobotTaskAssismentDto, RobotTaskAppServiceProxy } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/shared/common.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { copytaskComponent } from '../CopyTask/CopyTask.component';
import { MatDialog } from '@angular/material/dialog';
import { validationComponent } from '../Validation/validation.component';
import { VerificationComponent } from '../Verification/Verification.component';
import { yearsPerPage } from '@angular/material/datepicker';
import { documentComponent } from '../RObotTask/upload-document/upload-document.component';

@Component({
  selector: 'app-Matrix',
  styleUrls: ['./robot-assesmentmatrix.component.css'],
  templateUrl: './robot-assesmentmatrix.component.html',

})
export class RobotMatrixAssesment implements OnInit {

  constructor(
    private router: Router,
    private _robotTaskAssismentServiceProxy: RobotTaskAssismentServiceProxy,
    private _commonService: CommonService,
    private dialog: MatDialog,
    private _robotsystemAppServiceProxy: RobotTaskAppServiceProxy,


  ) { }
  trackingTableColumns: string[] = ['Edit', 'Delete', 'taskName', 'subTaskName', 'hazardName', 'riskMatrix_s', 'riskMatrix_E', 'riskMatrix_A', 'riskMatrix_R', 'riskMatrix_PLLcAT', 'safeguardsName', 'protectiveName', 'verification_S', 'verification_E', 'verification_A', 'comment', 'verification', 'validation', 'documentPath'];
  // trackingTableColumns: string[] = ['taskName', 'subTaskName', 'hazardName'];
  //  'S', 'E', 'A', 'R',  'Pl-Cat','S', 'E', 'A',

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  RobotTaskId: any;
  ngOnInit(): void {
    this.RobotTaskId = localStorage.getItem('RobotTaskId');
    this.GetByRobotId(this.RobotTaskId)
    this.getTaskAssisment(this.RobotTaskId);

  }
  AddNewTask() {
    this.router.navigate(['app/Add-Task']);
  }

  RobotTaskName: any;
  GetByRobotId(RobotTaskId) {
    this._robotsystemAppServiceProxy.getById(RobotTaskId).subscribe(result => {
      console.log(result);
      this.RobotTaskName = result[0].robotName;
    })
  }
  colourtemp: any[] = [];
  RobotAssismentData: RobotTaskAssismentDto[] = [];
  getTaskAssisment(RobotTaskId) {

    this._robotTaskAssismentServiceProxy.getRobotTaskAssisment(RobotTaskId).subscribe(result => {
      console.log(result);

      this.RobotAssismentData = result;
      this.dataSource = new MatTableDataSource(this.RobotAssismentData);
      this.dataSource.paginator = this.paginator;
      for (var i = 0; i < result.length; i++) {
        if (result[i].chklevel == "0" || result[i].chklevel == "1") {
          var temp11 = "Green"
        }
        else if (result[i].chklevel == "3" || result[i].chklevel == "4" || result[i].chklevel == "5" || result[i].chklevel == "9") {
          var temp11 = "Yellow"
        }
        else if (result[i].chklevel == "6" || result[i].chklevel == "7") {
          var temp11 = "GoldenRod"
        }
        else if (result[i].chklevel == "8" || result[i].chklevel == "10" || result[i].chklevel == "11") {
          var temp11 = "Red"
        }
        else if (result[i].chklevel == "12") {
          var temp11 = "Blue"
        }
        else if (result[i].chklevel == null) {
          var temp11 = "";
        }
        this.getColour(temp11, i)
      }


      for (var i = 0; i < result.length; i++) {
        if (result[i].documentName == "0" || result[i].documentName == "1") {
          var temp = "Green"
        }
        else if (result[i].documentName == "3" || result[i].documentName == "4" || result[i].documentName == "5" || result[i].chklevel == "9") {
          var temp = "Yellow"
        }
        else if (result[i].documentName == "6" || result[i].documentName == "7") {
          var temp = "GoldenRod"
        }
        else if (result[i].documentName == "8" || result[i].documentName == "10" || result[i].documentName == "11") {
          var temp = "Red"
        }
        else if (result[i].documentName == "12") {
          var temp = "Blue"
        }
        else if (result[i].documentName == null) {
          var temp = "";
        }
        this.getColour1(temp, i)
      }
    })
  }


  getColour(temp11, i) {
    this.RobotAssismentData[i].chkColor = temp11;

  }

  getColour1(temp, i) {
    this.RobotAssismentData[i].verificationchkColor = temp;

  }
  delete(id) {
    this._robotTaskAssismentServiceProxy.deleteTaskAssisment(id).subscribe(result => {
      this.getTaskAssisment(this.RobotTaskId);
      abp.notify.success('Deleted Successfully', 'Robot Assisment Task');
    })
  }

  EditTask(id) {
    localStorage.setItem('TaskAssismentId', id)
    var flag = "true";
    localStorage.setItem('TaskAssismentFlag', flag)

    this.router.navigate(['app/Add-Task']);
  }



  showcopyTaskDialog() {
    const dialogRef = this.dialog.open(copytaskComponent, {
      disableClose: true,
      height: '50%',
      width: '50%',

    });
  }

  showVarificationDialog() {
    const dialogRef = this.dialog.open(VerificationComponent, {
      disableClose: true,
      height: '50%',
      width: '50%',

    });
  }
  showvalidationDialog() {
    const dialogRef = this.dialog.open(validationComponent, {
      disableClose: true,
      height: '50%',
      width: '50%',

    });
  }
  colourset: any;
  DownLoad(){
    
    const dialogRef = this.dialog.open(documentComponent, {
      disableClose: true,
      height: '50%',
      width: '50%',

    });
  }
}