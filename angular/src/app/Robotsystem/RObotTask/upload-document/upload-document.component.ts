import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { HazardsComponent } from '@app/Parameters/hazards/hazards.component';
import { HazardServiceServiceProxy, RobotTaskAppServiceProxy, RobotTaskAssismentServiceProxy } from '@shared/service-proxies/service-proxies';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AnyMxRecord } from 'dns';
// import { RobotTaskAppServiceProxy, RobotSystem } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';



@Component({
  selector: 'app-uploaddcoument',
  templateUrl: './upload-document.component.html',
})
export class documentComponent implements OnInit {



  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<documentComponent>,
    private hazardservice: HazardServiceServiceProxy,
    private _robotTaskAppServiceProxy: RobotTaskAssismentServiceProxy,
  ) { }
  RobotTaskId: any;;
  ngOnInit(): void {

    this.RobotTaskId = localStorage.getItem('RobotTaskId');

    this.GetuploadDocument(this.RobotTaskId)
  }

  oldtaskD: any;
  newtaskD: any;

  close() {
    this.dialogRef.close();
  }
  RobotAssismentData: any;
  GetuploadDocument(RobotTaskId) {
    this._robotTaskAppServiceProxy.getByIdDcoument(RobotTaskId).subscribe(result => {
      this.RobotAssismentData = result;
      this.dataSource = new MatTableDataSource(this.RobotAssismentData);
      this.dataSource.paginator = this.paginator;
    })
  }

  DownLoad(data) {
    window.open(data)
  }

  deletd(id) {

  }
  trackingTableColumns: string[] = ['Delete', 'documentName', 'documentType', 'documentPath'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  public selectedFile: FileList;
  fileName: any;
  detectFile(event) {
    this.selectedFile = event.target.files;
    console.log(this.selectedFile);
    if (this.selectedFile != undefined) {
    }

  }

  Close() {
    this.dialogRef.close();
  }
  uploadSave() {
    this.uploaddocument()
  }

  uploaddocument() {
    Array.from(this.selectedFile).forEach(files => {
      var fileName = files.name;
      const file = { data: files, fileName: fileName }
      this._robotTaskAppServiceProxy.uploaddocument(this.RobotTaskId, file).subscribe(result => {
        this.GetuploadDocument(this.RobotTaskId)
      })
    })
  }
}
