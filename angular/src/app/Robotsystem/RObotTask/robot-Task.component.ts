import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { TaskServiceServiceProxy, SubtaskServiceServiceProxy, HazardServiceServiceProxy, SafeguardServiceServiceProxy, ProtectiveMeasureServiceServiceProxy, RobotTaskAppServiceProxy, RobotTaskAssismentServiceProxy, TaskDto, Task } from '@shared/service-proxies/service-proxies';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CreateTaskComponent } from '@app/Parameters/Task/create-task/create-task.component';
import { CreateSubtaskComponent } from '@app/Parameters/Subtask/create-subtask/create-subtask.component';
import { CreateSafeguardComponent } from '@app/Parameters/Safeguard/create-safeguard/create-safeguard.component';
import { CreateHazardsComponent } from '@app/Parameters/hazards/create-hazards/create-hazards.component';
import { CreateProtectivemeasureComponent } from '@app/Parameters/protectivemeasure/create-protectivemeasure/create-protectivemeasure.component';
import { filter } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { CommonService } from '@app/shared/common.service';
import { MatDialog } from '@angular/material/dialog';
import { validationComponent } from '../Validation/validation.component';
import { documentComponent } from './upload-document/upload-document.component';

@Component({
  selector: 'app-RobotTask',
  templateUrl: './robot-Task.component.html',

})
export class RobotTaskcomponent implements OnInit {

  constructor(
    private router: Router,
    private _robotTaskAppServiceProxy: RobotTaskAssismentServiceProxy,

    private _taskservice: TaskServiceServiceProxy,
    private _subtaskservice: SubtaskServiceServiceProxy,
    private _hazardservice: HazardServiceServiceProxy,
    private _safeguardServiceServiceProxy: SafeguardServiceServiceProxy,
    private _protectiveMeasureServiceServiceProxy: ProtectiveMeasureServiceServiceProxy,
    private _modalService: BsModalService,
    private _commonService: CommonService,
    private dialog: MatDialog,


  ) { }
  RobotTaskId: any;
  ngOnInit(): void {
    

    this.GetTask();
    this.GetsubTask();
    this.GetsHazard();
    this.Getssafegaurd();
    this.GetProtective();
    this.RobotTaskId = localStorage.getItem('RobotTaskId');

    var tempflag = localStorage.getItem('TaskAssismentFlag')

    if (tempflag == 'true') {
      this.GetEditByIdTask();

    }

     
  }

  TaskType: any;
  SubTaskType: any;
  HazardType: any;
  SafeguardsType: any;
  ProtectiveType: any;
  Comments: any;
  subTaskId: any;
  HazardId: any;
  safegaurdId: any;
  protectiveId: any;

  onTaskSelection(value) {
    console.log(value)
    this.taskId = value;
  }
  subtaskid: any[] = [];
  onsubTaskSelection(value) {
    // var temp = Array(value).toString()
    // var temp = Array(value.value[0].subtaskName).toString()
    // console.log(temp);

    // var tempid = Array(value.value[0].id).toString()
    // console.log(tempid);

    // // var temp1=this.subtaskData.filter(x=>x.subtaskName==Array(value).toString())
    // var temp1 = this.subtaskData.filter(x => x.subtaskName == Array(value).toString())[0].id
    // console.log(temp1);
    // value=''

    var temp = Array(value).toString()
    console.log(temp)
    this.subTaskId = temp;

  }

  onHazardSelection(value) {
    var temp = Array(value).toString()
    console.log(temp)
    this.HazardId = temp;
  }

  onsafegaurdSelection(value) {
    var temp = Array(value).toString()
    console.log(temp)
    this.safegaurdId = temp;
  }

  onProtectiveSelection(value) {
    var temp = Array(value).toString()
    console.log(temp)
    this.protectiveId = temp;
  }

  save(data, TaskAssimentId) {


    if (this.RiskMatrixBindarray == "CheckBox0") {
      data.riskMatrix_s = 'S1';
      data.riskMatrix_E = 'EO';
      data.riskMatrix_A = 'AO';
      data.riskMatrix_R = 'N';
      data.riskMatrix_PLLcAT = 'PL b - ';
      data.chklevel = '0';
    }
    else if (this.RiskMatrixBindarray == "CheckBox1") {
      data.riskMatrix_s = 'S1';
      data.riskMatrix_E = 'E1';
      data.riskMatrix_A = 'A1';
      data.riskMatrix_R = 'N';
      data.riskMatrix_PLLcAT = 'PL b - ';
      data.chklevel = '1'
    }
    else if (this.RiskMatrixBindarray == "CheckBox2") {
      data.riskMatrix_s = 'S1';
      data.riskMatrix_E = 'E2';
      data.riskMatrix_A = 'A2';
      data.riskMatrix_R = 'L';
      data.riskMatrix_PLLcAT = 'PL c Cat. 2';
      data.chklevel = '2';

    }
    else if (this.RiskMatrixBindarray == "CheckBox3") {
      data.riskMatrix_s = 'S1';
      data.riskMatrix_E = 'E2';
      data.riskMatrix_A = ' ';
      data.riskMatrix_R = 'L';
      data.chklevel = '3';
      data.riskMatrix_PLLcAT = 'PL c Cat. 2';
    }
    else if (this.RiskMatrixBindarray == "CheckBox4") {
      data.riskMatrix_s = 'S2';
      data.riskMatrix_E = 'EO';
      data.riskMatrix_A = ' ';
      data.riskMatrix_R = 'L';
      data.riskMatrix_PLLcAT = 'PL c Cat. 2';
      data.chklevel = '4';

    }
    else if (this.RiskMatrixBindarray == "CheckBox5") {
      data.riskMatrix_s = 'S2';
      data.riskMatrix_E = 'E1';
      data.riskMatrix_A = '';
      data.riskMatrix_R = 'M';
      data.riskMatrix_PLLcAT = 'PL d Cat. 2';
      data.chklevel = '5';

    }
    else if (this.RiskMatrixBindarray == "CheckBox6") {
      data.riskMatrix_s = 'S2';
      data.riskMatrix_E = 'E2';
      data.riskMatrix_A = 'A1';
      data.riskMatrix_R = 'M';
      data.riskMatrix_PLLcAT = 'PL d Cat. 2';
      data.chklevel = '6';

    }
    else if (this.RiskMatrixBindarray == "CheckBox7") {
      data.riskMatrix_s = 'S2';
      data.riskMatrix_E = 'E2';
      data.riskMatrix_A = 'A2';
      data.riskMatrix_R = 'H';
      data.riskMatrix_PLLcAT = 'PL d Cat. 3';
      data.chklevel = '7';

    }
    else if (this.RiskMatrixBindarray == "CheckBox8") {
      data.riskMatrix_s = 'S3';
      data.riskMatrix_E = 'EO';
      data.riskMatrix_A = '';
      data.riskMatrix_R = 'L';
      data.riskMatrix_PLLcAT = 'PL c Cat. 2';
      data.chklevel = '8';

    } else if (this.RiskMatrixBindarray == "CheckBox9") {
      data.riskMatrix_s = 'S3';
      data.riskMatrix_E = 'E1';
      data.riskMatrix_A = '';
      data.riskMatrix_R = 'L';
      data.riskMatrix_PLLcAT = 'PL d Cat. 3';
      data.chklevel = '9';

    }
    else if (this.RiskMatrixBindarray == "CheckBox10") {
      data.riskMatrix_s = 'S3';
      data.riskMatrix_E = 'E2';
      data.riskMatrix_A = 'A1/A2';
      data.riskMatrix_R = 'H';
      data.riskMatrix_PLLcAT = 'PL d Cat. 3';
      data.chklevel = '10';

    }

    else {
      data.riskMatrix_s = 'S3';
      data.riskMatrix_E = 'E2';
      data.riskMatrix_A = 'A3';
      data.riskMatrix_R = 'VH';
      data.riskMatrix_PLLcAT = 'PL e Cat. 4';
      data.chklevel = '11';

    }

    // Array.from(this.selectedFile).forEach(files => {
    //   data.verification = files.name;
    //   data.validation = files.type;
    // })



    data.robotTaskID = this.RobotTaskId;
    data.companyId = 1;
    data.taskId = this.taskId;
    data.subTaskId = this.subTaskId;
    data.hazardsId = this.HazardId;
    data.safeguardsId = this.safegaurdId;
    data.protectiveMeasures = this.protectiveId;
    data.comment = this.Comments;
    // data.documentName = files.name;
    // data.verification = files.type;

    if (TaskAssimentId == undefined) {
      this._robotTaskAppServiceProxy.create(data).subscribe(result => {

        abp.notify.success('Added Successfully', 'Robot Assisment Task');
        this.router.navigate(['app/RiskMatrix']);

        if (result != 0) {
          Array.from(this.selectedFile).forEach(files => {
            var fileName = files.name;
            const file = { data: files, fileName: fileName }
            // this.uploadDocument(file,result)


            this._robotTaskAppServiceProxy.uploaddocument(result, file).subscribe(result => {
            })
          })

        }


      })
    }
    else {
      console.log(data);
      data.id = TaskAssimentId;
      // data.taskId = 6;
      // data.subTaskId = 8;
      // data.hazardsId = 7
      // data.safeguardsId = 9;
      // data.protectiveMeasures = 7;
      // data.comment = this.Comments;
      this._robotTaskAppServiceProxy.taskAssismentUpdate(data).subscribe(result => {
        abp.notify.success('Updated Successfully', 'Robot Assisment Task');
        this.router.navigate(['app/RiskMatrix']);

        if (TaskAssimentId != 0) {
          Array.from(this.selectedFile).forEach(files => {
            var fileName = files.name;
            const file = { data: files, fileName: fileName }
            // this.uploadDocument(file,result)
            this._robotTaskAppServiceProxy.uploaddocument(TaskAssimentId, file).subscribe(result => {
              this.router.navigate(['app/RiskMatrix']);
            })
          })
        }

      })

    }
    // })

  }


  uploadDocument(file, id) {
    this._robotTaskAppServiceProxy.uploaddocument(file, id).subscribe(result => {
      this.router.navigate(['app/RiskMatrix']);
    })
  }
  taskData: any;
  taskD: any;
  taskId: any;
  GetTask() {
    this._taskservice.getAll().subscribe(result => {
      this.taskData = result;
      this.taskData1 = result;


    })
  }


  subtaskData: any;
  subtaskData1: any;

  subtaskD: any;

  GetsubTask() {
    this._subtaskservice.getAll().subscribe(result => {
      this.subtaskData = result;
      this.subtaskData1 = result;


    })
  }

  HazardData: any;
  HazardData1: any;

  hazardD: any;

  GetsHazard() {
    this._hazardservice.getAll().subscribe(result => {
      this.HazardData = result;
      this.HazardData1 = result;
    })
  }

  safegaurdData: any;
  safegaurdData1: any;
  safegaurdD: any;

  Getssafegaurd() {
    this._safeguardServiceServiceProxy.getAll().subscribe(result => {
      this.safegaurdData = result;
      this.safegaurdData1 = result;
    })
  }


  Protective: any;
  Protective1: any;
  ProtectiveD: any;

  GetProtective() {
    this._protectiveMeasureServiceServiceProxy.getAll().subscribe(result => {
      this.Protective = result;
      this.Protective1 = result;
    })
  }
  createtask() {
    this.showCreateOrEditTaskDialog();
  }


  private showCreateOrEditTaskDialog(): void {
    let createOrEditTaskDialog: BsModalRef;

    createOrEditTaskDialog = this._modalService.show(
      CreateTaskComponent,
      {
        class: 'modal-lg',
      }
    );
    // this._commonService.callMethodOfTaskRefreshComponent(true);
    this.GetTask();

  }
  createsubtask() {
    this.showCreateOrEditSubTaskDialog();
  }

  private showCreateOrEditSubTaskDialog(id?: number): void {
    let createOrEditSubTaskDialog: BsModalRef;

    createOrEditSubTaskDialog = this._modalService.show(
      CreateSubtaskComponent,
      {
        class: 'modal-lg',
      }
    );
    this.GetsubTask()
  }

  createsafegaurd() {
    this.showCreateOrEditHazardDialog();
  }

  private showCreateOrEditHazardDialog(id?: number): void {
    let createOrEditSubTaskDialog: BsModalRef;

    createOrEditSubTaskDialog = this._modalService.show(
      CreateSafeguardComponent,
      {
        class: 'modal-lg',
      }
    );

    this.Getssafegaurd()

  }

  createHazard() {
    this.showCreateOrEditHazaedDialog();
  }

  private showCreateOrEditHazaedDialog(): void {
    let createOrEdithazardDialog: BsModalRef;
    createOrEdithazardDialog = this._modalService.show(
      CreateHazardsComponent,
      {
        class: 'modal-lg',
      }
    );
    this.GetsHazard()

  }


  createProtectivemeasure() {
    this.showCreateOrEditsafetychainDialog()
  }

  private showCreateOrEditsafetychainDialog(id?: number): void {

    let createOrEdithazardDialog: BsModalRef;

    createOrEdithazardDialog = this._modalService.show(
      CreateProtectivemeasureComponent,
      {
        class: 'modal-lg',
      }
    );
    this.GetProtective()

  }
  BacktoRsikmatrix() {
    this.router.navigate(['app/RiskMatrix']);
  }
  subtaskTemp: any;
  updateflag: boolean = false;
  TaskAssimentId: any;
  GetEditByIdTask() {

    this.updateflag = true;
    var tempId = parseInt(localStorage.getItem('TaskAssismentId'))

    this._robotTaskAppServiceProxy.getRobotByIDTaskAssisment(tempId).subscribe(result => {
      this.taskD = parseInt(result[0].taskName);


      this.hazardD = result[0].hazardName.split(',')
      this.subtaskD = result[0].subTaskName.split(',')

      this.safegaurdD = result[0].safeguardsName.split(',');
      this.ProtectiveD = result[0].protectiveName.split(',')
      this.TaskAssimentId = result[0].id;
      this.Comments = result[0].comment;
      if (result[0].chklevel == null) {
      }
      else {
        this.RiskMatrixArray[result[0].chklevel].ischecked = true
      }
      localStorage.removeItem('TaskAssismentId')
      localStorage.removeItem('TaskAssismentFlag')
    })
  }

  Update() {
  }

  RiskMatrixArray: any[] = [
    { ID: 'CheckBox0', ischecked: false },
    { ID: 'CheckBox1', ischecked: false },
    { ID: 'CheckBox2', ischecked: false },
    { ID: 'CheckBox3', ischecked: false },
    { ID: 'CheckBox4', ischecked: false },
    { ID: 'CheckBox5', ischecked: false },
    { ID: 'CheckBox6', ischecked: false },
    { ID: 'CheckBox7', ischecked: false },
    { ID: 'CheckBox8', ischecked: false },
    { ID: 'CheckBox9', ischecked: false },
    { ID: 'CheckBox10', ischecked: false },
    { ID: 'CheckBox11', ischecked: false },
  ]


  RiskMatrixBindarray: any;
  EarlyCheckForm: any;


  onEarlyChange(event) {
    this.EarlyCheckForm = event.checked
    // //console.log(event.source.name);

    const checkedElementName = event.source.name
    this.RiskMatrixBindarray = event.source.name
    const isAlreadyChecked = this.RiskMatrixArray.map(i => i.ID).indexOf(checkedElementName) === -1

    if (isAlreadyChecked) {
    }
    else {
      this.RiskMatrixArray = this.RiskMatrixArray.map(i => {
        return {
          ID: i.ID,
          ischecked: i.ID !== checkedElementName ? false : true
        }
      });
    }
  }
  taskData1: any;
  // applyFilter(filterValue) {
  //   console.log(filterValue);

  //   filterValue = filterValue.trim();
  //   filterValue = filterValue.toLowerCase();
  //   this._taskservice.getAll().subscribe(result => {
  //     this.taskData1 = result;
  //     this.taskData = this.taskData1.filter(x => x.taskName == filterValue);
  //     console.log(this.taskData);

  //   })


  selectedvalue: any;
  // applyFilter(value) {
  //   this.selectedvalue = this.search(value);
  // }

  search(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    return this.taskData.filter(x => x.taskName == filterValue);
  }
  applyFilter(filterValue: string) {
    this.taskData = this.taskData1.filter((i) => i.taskName.toLowerCase().includes(filterValue.toLowerCase()));
  }

  applyFiltersubtask(filterValue1: string) {
    console.log(filterValue1);
    this.subtaskData = this.subtaskData1.filter((i) => i.subtaskName.toLowerCase().includes(filterValue1.toLowerCase()));
  }


  applyFilterhazard(filterValue1: string) {
    console.log(filterValue1);
    this.HazardData = this.HazardData1.filter((i) => i.hazName.toLowerCase().includes(filterValue1.toLowerCase()));

  }
  applyFiltersafe(filterValue1: string) {
    console.log(filterValue1);
    this.safegaurdData = this.safegaurdData1.filter((i) => i.safeguardName.toLowerCase().includes(filterValue1.toLowerCase()));

  }
  applyFilterprotective(filterValue1: string) {
    console.log(filterValue1);
    this.Protective = this.Protective1.filter((i) => i.protectiveName.toLowerCase().includes(filterValue1.toLowerCase()));

  }
  public selectedFile: FileList;
  fileName: any;
  detectFile(event) {
    this.selectedFile = event.target.files;
    console.log(this.selectedFile);

    if (this.selectedFile != undefined) {

    }

  }
  TaskDetails = new Task();
  uploaddcoument(){
    const dialogRef = this.dialog.open(documentComponent, {
      disableClose: true,
      height: '50%',
      width: '50%',

    });
  }
}
