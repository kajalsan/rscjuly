import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { MaterialModule } from '../shared/matarial.module';
import { AppModule } from './../app.module';
import { TaskServiceServiceProxy, RobotTask, RobotTaskAssismentServiceProxy, RobotTaskAppServiceProxy } from '../../shared/service-proxies/service-proxies';
import { SharedModule } from '../../shared/shared.module';
import { robotsystemaddComponent } from './robotsystemadd.component';
import { RobotMatrixAssesment } from './robot-assesmentmatrix/robot-assesmentmatrix.component';
import { RouterModule } from '@angular/router';
import { RobotsystemRouter } from './RobotSystem.routing';
import { RobotTaskcomponent } from './RObotTask/robot-Task.component';
import { CommonService } from '../shared/common.service';
import { SafetyRequirementComponent } from './safety-requirement/safety-requirement.component';

@NgModule({
  declarations: [
    robotsystemaddComponent,
    RobotMatrixAssesment,
    RobotTaskcomponent,
    SafetyRequirementComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forChild(),
    RouterModule.forChild(RobotsystemRouter),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    NgxPaginationModule,
    MaterialModule,
    AppModule,
    SharedModule
  ],
  providers: [RobotTaskAssismentServiceProxy, RobotTaskAppServiceProxy, CommonService],

  entryComponents: [RobotTaskcomponent],

  // schemas: [CUSTOM_ELEMENTS_SCHEMA]

})


export class RobosystemModule { }
