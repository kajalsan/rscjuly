import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnsharedComponent } from './unshared.component';

describe('UnsharedComponent', () => {
  let component: UnsharedComponent;
  let fixture: ComponentFixture<UnsharedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnsharedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnsharedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
