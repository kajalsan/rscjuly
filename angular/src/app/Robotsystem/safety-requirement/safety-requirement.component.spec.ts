import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyRequirementComponent } from './safety-requirement.component';

describe('SafetyRequirementComponent', () => {
  let component: SafetyRequirementComponent;
  let fixture: ComponentFixture<SafetyRequirementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetyRequirementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyRequirementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
