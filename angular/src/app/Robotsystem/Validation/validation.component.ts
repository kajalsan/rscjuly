import { Component, OnInit, NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { HazardsComponent } from '@app/Parameters/hazards/hazards.component';
import { HazardServiceServiceProxy } from '@shared/service-proxies/service-proxies';
// import { RobotTaskAppServiceProxy, RobotSystem } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';



@Component({
  selector: 'app-validation',
  templateUrl: './Validation.component.html',
})
export class validationComponent implements OnInit {



  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<validationComponent>,
    private hazardservice: HazardServiceServiceProxy

  ) { }

  ngOnInit(): void {

   // this.GetTask();
  }
  oldtaskD: any;
  newtaskD: any;

  close() {
    this.dialogRef.close();


  }
  taskId: any;
  onTaskSelection(value) {
    console.log(value)
    this.taskId = value;
  }
  taskData: any;
  boxon: boolean = false;
  random: any;
  hazdata: any;
  boxcre() {
    this.boxon = true;
    this.random = Math.floor(Math.random() * (999999 - 100000)) + 100000;
    alert(this.random)
    this.hazdata={
      'hazName' : this.random,
    'companyId': 18
    }

    this.hazardservice.create(this.hazdata).subscribe(result => {
      this.GetTask();
    })
  }
  slectedBox: Boolean = false
  selected(hazn) {
    alert('test')
    this.slectedBox = hazn
  }
  GetTask() {
    this.hazardservice.getAll().subscribe(result => {
      this.taskData = result;

    })
  }

  deletd(id){
    alert(id)
    this.hazardservice.delete(id).subscribe(res=>{
this.GetTask()
    })
  }
}
