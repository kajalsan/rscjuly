import { Component, OnInit, NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { TaskServiceServiceProxy, RobotTaskAppServiceProxy, RobotTaskAssismentServiceProxy } from '@shared/service-proxies/service-proxies';
import { filter } from 'rxjs/operators';
// import { RobotTaskAppServiceProxy, RobotSystem } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';



@Component({
  selector: 'app-CopyTask',
  templateUrl: './copytask.compoet.html',
})
export class copytaskComponent implements OnInit {



  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<copytaskComponent>,
    private _taskservice: TaskServiceServiceProxy,
    private _RobotTaskAppServiceProxy: RobotTaskAssismentServiceProxy,


  ) { }

  ngOnInit(): void {
    this.RobotTaskId = localStorage.getItem('RobotTaskId');
    this.GetTask();
    this.GetOldTask();
  }
  oldtaskD: any;
  newtaskD: any;

  close() {
    this.dialogRef.close();
  }
  taskId: any;
  onTaskSelection(value) {
    console.log(value)
    this.taskId = value;

  }
  tasknewData: any;
  taskoldData: any;
  taskNew:any;
  onnewTaskSelection(value){
    console.log(value)
    this.taskNew = value;

  }

  GetTask() {
    this._taskservice.getAll().subscribe(result => {
      // this.tasknewData = result;
      this._RobotTaskAppServiceProxy.getRobotTaskAssisment(this.RobotTaskId).subscribe(res => {
        for (var i = 0; i < result.length; i++) {
          this.tasknewData = result.filter(x => x.id != parseInt(res[i].taskId));
        }
        // Push.this.tasknewData(temp);
      })
    })
  }


  GetOldTask() {
    this._RobotTaskAppServiceProxy.getRobotTaskAssisment(this.RobotTaskId).subscribe(result => {
      this.taskoldData = result;

    })
  }

  RobotTaskId: any;
  save(data) {
    this._RobotTaskAppServiceProxy.getRobotByIDTaskAssisment(this.taskId).subscribe(result => {
      this.taskoldData = result;

      // let data
      data.companyId = 1;
      data.taskId = this.taskNew;
      data.subTaskId = result[0].subTaskName;
      data.hazardsId = result[0].hazardName;
      data.safeguardsId = result[0].safeguardsName;
      data.protectiveMeasures = result[0].protectiveName;
      data.comment = result[0].comment;
      data.robotTaskID = this.RobotTaskId;
      // data.chklevel = result[0].chklevel;
      // data.documentPath = result[0].documentPath;

      // data.riskMatrix_s = result[0].riskMatrix_s;
      // data.riskMatrix_E = result[0].riskMatrix_E; 
      //  data.riskMatrix_A = result[0].riskMatrix_A;
      // data.riskMatrix_R = result[0].riskMatrix_R;
      // data.riskMatrix_PLLcAT = result[0].riskMatrix_PLLcAT;

      


      this._RobotTaskAppServiceProxy.create(data).subscribe(result => {
        this.dialogRef.close();
        abp.notify.success('Added Successfully', 'Copy Task');

      })
    })
  }


}
