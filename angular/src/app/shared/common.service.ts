import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class CommonService {

    private invokeTaskRefresh = new Subject<boolean>();
  
    TaskRefreshEvent = this.invokeTaskRefresh.asObservable();


    constructor() { }

    callMethodOfTaskRefreshComponent(boolean) {
        this.invokeTaskRefresh.next(boolean);
    }
}
