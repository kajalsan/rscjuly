import { Component, OnInit } from '@angular/core';
import { RegisterInput } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  model1: RegisterInput = new RegisterInput();

  saving = false;
  value: any;
  users: string;
  selectedplan: any;
  companyId:any;
  Cname: string;
  pnumber: string;
  address: string;
  userBasicInformation: any;
  select_plan: string;
  username: string;
  password: string;
  surname: string;
  emailID: string;
  name: string;

  constructor() { }

  ngOnInit(): void {

    this.username = localStorage.getItem('uname');
    this.password = localStorage.getItem('password');
    this.name = localStorage.getItem('name');
    this.surname = localStorage.getItem('surname');
    this.emailID = localStorage.getItem('emailAddress');
    
  }


}
