import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { UserServiceProxy, UserDto, CreateUserDto, RoleDto, UserDtoPagedResultDto, UserportalAppServiceProxy, UserPortal, CompanyAppServiceProxy, LocationAppServiceProxy, EmailerServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppSessionService } from '@shared/session/app-session.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { PagedRequestDto } from '@shared/paged-listing-component-base';
import { AppComponentBase } from '@shared/app-component-base';

class PagedUsersRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}


@Component({
  selector: 'app-user-portal',
  templateUrl: './user-portal.component.html',
  styleUrls: ['./user-portal.component.css']
})
export class UserPortalComponent extends AppComponentBase implements OnInit {
  model: UserDto = new UserDto();
  users: UserDto[] = [];
  Userp: UserPortal = new UserPortal();
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  name: any;
  emailID: string;
  saving = false;
  user = new CreateUserDto();
  roles: RoleDto[] = [];
  checkedRolesMap: { [key: string]: boolean } = {};
  defaultRoleCheckedStatus = false;
  UserDetails: UserPortal[];
  RoleName: string;
  surname: string;
  UserId: number;
  companyId: number;
  subscriptiontype: string;
  Userlicenses: number;
  remaining: any;
  calculation: number;
  UserportalId: any;
  upgrade:boolean = false;

  @ViewChild('userclosebutton') userclosebutton;
  @ViewChild('edituserclosebutton') edituserclosebutton;



  constructor(
    private injector: Injector, private userservice: UserServiceProxy,
    private appsession: AppSessionService,
    private UserportalService: UserportalAppServiceProxy,
    private compservice: CompanyAppServiceProxy,
    private locservice: LocationAppServiceProxy,
    private emailservice: EmailerServiceProxy) {
    super(injector)
  }

  ngOnInit(): void {

    this.UserId = +localStorage.getItem("registerId");

    this.companyId = +localStorage.getItem('CompanyId');

    this.appsession.user;

    console.log(this.appsession.user);

    this.name = this.appsession.user.name;

    this.surname = this.appsession.user.surname;

    this.emailID = this.appsession.user.emailAddress;

    this.user.isActive = true;

    this.RoleName = "ADMIN"

    this.GetDetails();

    this.compservice.getcompanyForEdit(this.companyId).subscribe((res) => {
      this.subscriptiontype = res.subscriptiontype;
    })

    if (this.subscriptiontype == 'Free') {
      this.Userlicenses = 2;
    }

    else {
      this.Userlicenses = 5;
    }

    this.GetcountData();

  }

  userform(data) {
    data.locationId = 1;
    data.userId = 23;
    data.CompanyId = 5;

    this.UserportalService.create(data).subscribe((res) => {
      this.UserportalId = res;

      this.GetDetails();
      this.userclosebutton.nativeElement.click();
      this.emailservice.createuserpassEmail(this.Userp.emailAddress).subscribe((res) => {
        this.notify.success(this.l(' Send Mail Successfully'));
      })
      this.notify.success(this.l(' Added User Successfully'));
      localStorage.setItem('UserportalId', this.UserportalId.toString());
    })

  }


  GetDetails() {
    this.UserportalService.getAll().subscribe((res) => {
      this.UserDetails = res
      this.GetcountData();
      console.log(res)
    })
  }

  GetcountData() {
    this.UserportalService.getCountById().subscribe((res) => {
      this.remaining = res;
      console.log(this.Userlicenses);
      console.log(this.remaining);
      this.calculation = this.Userlicenses - this.remaining - 1;
      console.log(this.calculation, "calcuation");

      if(this.calculation==0)
      {
        this.upgrade = true;
      }
})

   
  }

  Edituserform(data) {

    this.UserportalId = localStorage.getItem('UserportalId');
    //data = this.Userp;
    data.locationId = 1;
    data.userId = 23;
    data.CompanyId = 5;

    this.UserportalService.update(this.UserportalId, data).subscribe((res) => {
      this.GetDetails();
      this.edituserclosebutton.nativeElement.click();

    })

  }

}
