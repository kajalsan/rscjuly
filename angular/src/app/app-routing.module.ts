import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { TaskComponent } from './Parameters/Task/task.component';
import { ExportParametersComponent } from './Parameters/export-parameters/export-parameters.component';
import { CreateTaskComponent } from './Parameters/Task/create-task/create-task.component';
import { CreateHazardsComponent } from './Parameters/hazards/create-hazards/create-hazards.component';
import { CreateProtectivemeasureComponent } from './Parameters/protectivemeasure/create-protectivemeasure/create-protectivemeasure.component';
import { CreateSafeguardComponent } from './Parameters/Safeguard/create-safeguard/create-safeguard.component';
import { CreateSubtaskComponent } from './Parameters/Subtask/create-subtask/create-subtask.component';
import { SubtaskComponent } from './Parameters/Subtask/subtask.component';
import { SetupComponent } from './setup/setup.component';
import { ProfileComponent } from './profile/profile.component';
import { UserPortalComponent } from './user-portal/user-portal.component';
import { HazardsComponent } from './Parameters/hazards/hazards.component';
import { ProtectivemeasureComponent } from './Parameters/protectivemeasure/protectivemeasure.component';
import { from } from 'rxjs';
import { robotsystemaddComponent } from './Robotsystem/robotsystemadd.component';
import { RobotMatrixAssesment } from './Robotsystem/robot-assesmentmatrix/robot-assesmentmatrix.component';
import { RobotTaskcomponent } from './Robotsystem/RObotTask/robot-Task.component';
import { SafeguardComponent } from './Parameters/Safeguard/safeguard.component';
import { VerificationTaskComponent } from './Robotsystem/Verification/VerificationTask/VerificationTask.component';
import { VerificationpunchComponent } from './Robotsystem/Verification/VerificationPunch/Verificationpunch.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LocationsComponent } from './locations/locations.component';
import { ExampleComponent } from './example/example.component';
import { CreateuserpassComponent } from './createuserpass/createuserpass.component';
import { SharedComponent } from './Robotsystem/shared/shared.component';
import { UnsharedComponent } from './Robotsystem/unshared/unshared.component';
import { updateRobotComponent } from './Robotsystem/updateRobot/updateRobot.component';
import { VerificationriskMatrixComponent } from './Robotsystem/Verification/verificationRiskMatrix/verificationRiskMatrix.component';
import { SafetyRequirementComponent } from './Robotsystem/safety-requirement/safety-requirement.component';

 
@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                children: [
                    { path: 'home', component: HomeComponent, canActivate: [AppRouteGuard] },
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' }, canActivate: [AppRouteGuard] },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
                    { path: 'about', component: AboutComponent },
                    { path: 'update-password', component: ChangePasswordComponent },

                    { path: 'dashboard', component: DashboardComponent },
                    // { path: 'task', loadChildren: './Parameters/Task/task.module#TaskModule' },
                    { path: 'Locations', component: LocationsComponent },
                    { path: 'example', component: ExampleComponent },

                    { path: 'task', component: TaskComponent },
                    { path: 'task/addTask', component: CreateTaskComponent },

                    { path: 'subtask', component: SubtaskComponent },
                    { path: 'subtask/addSubtask', component: CreateSubtaskComponent },

                    { path: 'hazard', component: HazardsComponent },
                    { path: 'hazard/addHazard', component: CreateHazardsComponent },

                    { path: 'protectivemeasure', component: ProtectivemeasureComponent },
                    { path: 'protectivemeasure/addProtectivemeasure', component: CreateProtectivemeasureComponent },

                    { path: 'safeguard', component: SafeguardComponent },
                    { path: 'safeguard/addSafeguard', component: CreateSafeguardComponent },

                    { path: 'export-parameters', component: ExportParametersComponent },

                    { path: 'setup', component: SetupComponent },
                    { path: 'profile', component: ProfileComponent },
                    { path: 'userprotal', component: UserPortalComponent },
                    { path: 'userpass', component: CreateuserpassComponent },


                    { path: 'add-robot', component: robotsystemaddComponent },
                    { path: 'RiskMatrix', component: RobotMatrixAssesment },
                    { path: 'Add-Task', component: RobotTaskcomponent },
                    { path: 'Verificationtask', component: VerificationTaskComponent },
                    { path: 'Verification-punch', component: VerificationpunchComponent },
                    
                    { path: 'update-robot', component: updateRobotComponent },
 
                    { path: 'verification-riskmatrix', component: VerificationriskMatrixComponent },
                    
 
                    { path: 'share', component: SharedComponent },
                    { path: 'unshare', component: UnsharedComponent },

                    { path: 'safetyrequire', component: SafetyRequirementComponent },

                    // { path: 'safetyrequirement', component: SafetyRequirementComponent },


                    // { path: 'add-robot', loadChildren: './Robotsystem/RobotSystem.module#robotModule'},

                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
