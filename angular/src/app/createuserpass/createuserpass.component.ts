import {Component, Injector, OnInit} from '@angular/core';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';
import { UserPortal, UserportalAppServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { Router } from '@angular/router';


@Component({
    selector:'userpass',
    templateUrl:'./createuserpass.component.html',
   styleUrls:['./createuserpass.component.css']

})


export class CreateuserpassComponent extends AppComponentBase implements OnInit
{

    Userp: UserPortal = new UserPortal();

    passwordTextType: boolean;
    confirmpassTextType : boolean;
    UserportalId: any;


  


    constructor(private injector:Injector,
        private UserportalService:UserportalAppServiceProxy,
        private router:Router){
        super(injector)
    }

    passwordFieldTextType() {
        this.passwordTextType = !this.passwordTextType;
      }
    
      confirmpasswordFieldTextType() {
        this.confirmpassTextType = !this.confirmpassTextType;
      }
    
      passwordValidationErrors: Partial<AbpValidationError>[] = [
        {
          name: 'pattern',
          localizationKey:
            'PasswordsMustBeAtLeast8CharactersContainLowercaseUppercaseNumber',
        },
      ];
      confirmPasswordValidationErrors: Partial<AbpValidationError>[] = [
        {
          name: 'validateEqual',
          localizationKey: 'PasswordsDoNotMatch',
        },
      ];


      ngOnInit()
      {
        this.UserportalId = localStorage.getItem('UserportalId');
      }

     
      userpass(data){

        // this.UserportalService.updatepass(this.UserportalId, data).subscribe((res)=>{
        //     this.notify.success(this.l("Password Created Successfully..!!"));
        //     this.router.navigate(['/account/login']);
        // })
  }
  
}