import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { HomeComponent } from '@app/home/home.component';
import { AboutComponent } from '@app/about/about.component';
// tenants
import { TenantsComponent } from '@app/tenants/tenants.component';
import { CreateTenantDialogComponent } from './tenants/create-tenant/create-tenant-dialog.component';
import { EditTenantDialogComponent } from './tenants/edit-tenant/edit-tenant-dialog.component';
// roles
import { RolesComponent } from '@app/roles/roles.component';
import { CreateRoleDialogComponent } from './roles/create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './roles/edit-role/edit-role-dialog.component';
// users
import { UsersComponent } from '@app/users/users.component';
import { CreateUserDialogComponent } from '@app/users/create-user/create-user-dialog.component';
import { EditUserDialogComponent } from '@app/users/edit-user/edit-user-dialog.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { ResetPasswordDialogComponent } from './users/reset-password/reset-password.component';
// layout
import { HeaderComponent } from './layout/header.component';
import { HeaderLeftNavbarComponent } from './layout/header-left-navbar.component';
import { HeaderLanguageMenuComponent } from './layout/header-language-menu.component';
import { HeaderUserMenuComponent } from './layout/header-user-menu.component';
import { FooterComponent } from './layout/footer.component';
import { SidebarComponent } from './layout/sidebar.component';
import { SidebarLogoComponent } from './layout/sidebar-logo.component';
import { SidebarUserPanelComponent } from './layout/sidebar-user-panel.component';
import { SidebarMenuComponent } from './layout/sidebar-menu.component';
import { TaskModule } from './Parameters/Task/task.module';
import { SubTaskModule } from './Parameters/Subtask/Subtask.module';
import { HazardsModule } from './Parameters/hazards/hazards.module';
import { ProtectiveMeasureModule } from './Parameters/protectivemeasure/protectiveMeasure.module';
import { SafeguardModule } from './Parameters/Safeguard/safeguard.module';
// import { TaskServiceServiceProxy, SubtaskServiceServiceProxy } from '@shared/service-proxies/service-proxies';
import { MaterialModule } from './shared/matarial.module';
import { SetupComponent } from './setup/setup.component';
import { ProfileComponent } from './profile/profile.component';
import { UserPortalComponent } from './user-portal/user-portal.component';
import { from } from 'rxjs';
import { robotsystemaddComponent } from './Robotsystem/robotsystemadd.component';
import { RobotTaskcomponent } from './Robotsystem/RObotTask/robot-Task.component';
import { LocationsComponent } from './locations/locations.component';
import { CommonService } from './shared/common.service';
import { MatTableModule } from '@angular/material/table';
import { RobotMatrixAssesment } from './Robotsystem/robot-assesmentmatrix/robot-assesmentmatrix.component';
import { copytaskComponent } from './Robotsystem/CopyTask/CopyTask.component';
import { validationComponent } from './Robotsystem/Validation/validation.component';
import { VerificationComponent } from './Robotsystem/Verification/Verification.component';
import { VerificationTaskComponent } from './Robotsystem/Verification/VerificationTask/VerificationTask.component';
import { VerificationpunchComponent } from './Robotsystem/Verification/VerificationPunch/Verificationpunch.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AgmCoreModule } from '@agm/core';
import { PhoneMaskDirective } from './phone-mask.directive';
import { ExportParametersComponent } from './Parameters/export-parameters/export-parameters.component';
import { ExampleComponent } from './example/example.component';
 
import { updateRobotComponent } from '@app/Robotsystem/updateRobot/updateRobot.component'
import { VerificationriskMatrixComponent } from './Robotsystem/Verification/verificationRiskMatrix/verificationRiskMatrix.component';
import { documentComponent } from '././Robotsystem/RObotTask/upload-document/upload-document.component';
 
import {CreateuserpassComponent} from './createuserpass/createuserpass.component';
import { RobosystemModule } from './Robotsystem/RobotSystem.module';

  

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    // tenants
    TenantsComponent,
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    UsersComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ChangePasswordComponent,
    ResetPasswordDialogComponent,
    // layout
    HeaderComponent,
    HeaderLeftNavbarComponent,
    HeaderLanguageMenuComponent,
    HeaderUserMenuComponent,
    FooterComponent,
    SidebarComponent,
    SidebarLogoComponent,
    SidebarUserPanelComponent,
    SidebarMenuComponent,
    SetupComponent,
    ProfileComponent,
    UserPortalComponent,
    LocationsComponent,  
    // robotsystemaddComponent,
    // RobotTaskcomponent,
    // RobotMatrixAssesment,
    copytaskComponent,
    validationComponent,
    VerificationComponent,
    VerificationTaskComponent,
    VerificationpunchComponent,
    DashboardComponent,
    ExportParametersComponent,
    ExampleComponent,
    updateRobotComponent,
    VerificationriskMatrixComponent,
    documentComponent,
    updateRobotComponent

  ],
  imports: [
    MaterialModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forChild(),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule,
    // RobosystemModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDeBg1S6PN6R8CJkzCgv-qodYjEOJnBH1o',
      libraries: ['places']
    }),
    // LocationsModuleV
    MatTableModule,
  ],
  providers: [CommonService],
  entryComponents: [
    // tenants
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ResetPasswordDialogComponent,


  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
