import { Component, OnInit, Injector, NgZone, ElementRef, ViewChild } from '@angular/core';
import {LocationAppServiceProxy, Locations, CompanyAppServiceProxy } from '../../shared/service-proxies/service-proxies';
import {AppComponentBase} from '../../shared/app-component-base'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css']
})
  export class LocationsComponent extends AppComponentBase implements OnInit {

  model: Locations = new Locations();
  saving = false;
  companyId: number;
  userDetails: Locations[];

  @ViewChild('search') public searchElementRef: ElementRef;
  address: string;
  latitude: number;
  longitude: number;
  zoom: number;
  compLoc: any;
  
  constructor( private locationservice:LocationAppServiceProxy,
    private injector:Injector,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, 
    private companyservice : CompanyAppServiceProxy) { 
    super(injector)
  }

  ngOnInit(): void {
    this.companyId = +localStorage.getItem('CompanyId');
    this.GetAllDetails();
    this.GetbyId()
  }

  ngAfterViewInit() {
    this.findAdress();
  }
  findAdress() {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  GetAllDetails() {
    this.locationservice.getAll(this.companyId).subscribe((res) => {
      this.userDetails = res;
    })
  }

  GetbyId(){
    this.companyservice.getcompanyForEdit(this.companyId).subscribe((res)=>{
        this.compLoc = res;
        console.log(res);

    })
  }

  save(data)
  {
    this.saving = true;
    this.model.companyId = +localStorage.getItem('CompanyId')
      this.locationservice.create(data).subscribe((res)=>
      {
         this.notify.success(this.l('Added Successfully'));
      })
  }



}
