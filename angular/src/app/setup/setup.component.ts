import { Component, OnInit, NgZone, ViewChild, ElementRef, Injector, EventEmitter, Output } from '@angular/core';
import { RegisterInput, CompanyAppServiceProxy, Company, UserServiceProxy, UserDto, AccountServiceProxy } from '@shared/service-proxies/service-proxies';
import { MapsAPILoader } from '@agm/core';
import { AppComponentBase } from '@shared/app-component-base';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';
import { CommonService } from '@app/shared/common.service';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.css']
})
export class SetupComponent extends AppComponentBase implements OnInit {
  model1: Company = new Company();
  model: RegisterInput = new RegisterInput();
  model2: UserDto = new UserDto();
  subscriptiondiv: Boolean = true;
  username: string;
  password: string;
  name: string;
  surname: string;
  emailID: string;
  companyName: any;
  companyId: any;
  phoneno: string;
  address: string;
  latitude: number;
  longitude: number;
  zoom: number;
  UserId: number;

  @ViewChild('search') public searchElementRef: ElementRef;

  @ViewChild('companyclosebutton') companyclosebutton;

  @ViewChild('profileclosebutton') profileclosebutton;

  @ViewChild('emailidclosebutton') emailidclosebutton;

  @ViewChild('passwordclosebutton') passwordclosebutton;

  @ViewChild('addressclosebutton') addressclosebutton;

  // passwordValidationErrors: Partial<AbpValidationError>[] = [
  //   {
  //     name: 'pattern',
  //     localizationKey:
  //       'PasswordsMustBeAtLeast8CharactersContainLowercaseUppercaseNumber',
  //   },
  // ];
  // confirmPasswordValidationErrors: Partial<AbpValidationError>[] = [
  //   {
  //     name: 'validateEqual',
  //     localizationKey: 'PasswordsDoNotMatch',
  //   },
  // ];

  @Output() onSave = new EventEmitter<any>();
  confirmPassword: string;
  addresss: string;

  constructor(private companysevice: CompanyAppServiceProxy,
    injector: Injector,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private userservice: UserServiceProxy,
    private accountservice: AccountServiceProxy,
    private commonservice:CommonService) { super(injector) }

  ShowHideButton() {
    this.subscriptiondiv = this.subscriptiondiv ? false : true;
  }
  ngOnInit(): void {

    this.companyId = +localStorage.getItem('CompanyId');

    this.UserId = +localStorage.getItem("registerId")

    this.username = localStorage.getItem('uname');
    this.password = localStorage.getItem('password');
    this.confirmPassword = localStorage.getItem('confirmPassword')
    // this.name = localStorage.getItem('name');
    // this.surname = localStorage.getItem('surname');
    // this.emailID = localStorage.getItem('emailAddress');
    this.model1.userId = +localStorage.getItem("registerId");


    this.companysevice.getcompanyForEdit(this.companyId).subscribe((res) => {
      this.model1 = res;
      console.log(res);

      this.companyName = res.companyName;
      this.phoneno = res.phoneno;
      this.addresss = res.address;
    })

  
    // this.accountservice.getbyId(this.UserId).subscribe((res)=>
    // {
    //   this.model = res;
     
    //   this.model.password = this.password;
    //  this.model.confirmPassword =  this.model.password;
    //   // this.model.userName = this.username;

    //   this.password = this.model.password;
    //   this.confirmPassword =  this.model.confirmPassword;
    //   this.username = "ADMIN";
    //   // this.password = "Test@123";
    //   // this.confirmPassword = "Test@123";
    //   this.name = this.model.name;
    //   this.surname = this.model.surname;
    //   this.emailID = this.model.emailAddress;

    //   console.log(res,"Account");

    // })

    this.appSession.user;

    this.name = this.model.name;
      this.surname = this.appSession.user.surname;
      this.emailID = this.appSession.user.emailAddress;
      //   this.password = this.appSession.user.password;
      // this.confirmPassword =  this.appSession.user.confirmPassword;

    this.username = "ADMIN";


  }
  ngAfterViewInit() {
    this.findAdress();
  }
  findAdress() {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  Updatecomapany(data: any) {

    this.UserId = this.model1.userId;
    data = this.model1;

    this.companysevice.update(this.companyId, data).subscribe((res) => {
      this.notify.info(this.l('Update Successfully'));
      this.companyclosebutton.nativeElement.click();
      this.companyName = data.companyName;
      console.log(res);
    })
  }

  UpdatecomapanyAddress(data: any) {

    this.UserId = this.model1.userId;
    data = this.model1;

    this.companysevice.updateAddress(this.companyId, data).subscribe((res) => {
      this.notify.info(this.l('Update Address Successfully'));
      this.addressclosebutton.nativeElement.click();
      this.addresss = data.address;
      console.log(res);
    })
  }

  Updateprofile(data: any) {

    this.model.confirmPassword = this.confirmPassword;
    data = this.model;

    this.accountservice.update(this.UserId,data).subscribe((res) => {
      this.notify.info(this.l('Update Successfully'));
      this.profileclosebutton.nativeElement.click();
      this.name = data.name;
      this.surname = data.surname;
      console.log(res);
    });
  }

  UpdateprofileEmail(data: any) {
    this.model.userName = "ADMIN";
  data = this.model;

    this.accountservice.update(this.UserId,data).subscribe((res) => {
      this.notify.info(this.l('Update Successfully'));
      this.emailidclosebutton.nativeElement.click();
      this.emailID = data.emailAddress;
      console.log(res);

    });
  }

  Updatepassword(data: any) {
    data = this.model;

    this.accountservice.update(this.UserId,data).subscribe((res) => {
      this.notify.info(this.l('Update Successfully'));
      this.passwordclosebutton.nativeElement.click();
      console.log(res);
    });
  }
}
